<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'UserDetailsId')->textInput() ?>

    <?= $form->field($model, 'DocumentName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Document')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DocumentValue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IsDelete')->textInput() ?>

    <?= $form->field($model, 'Ondate')->textInput() ?>

    <?= $form->field($model, 'UpdateDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
