<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Userdetails */

$this->title = 'Update Userdetails: ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Userdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->UserDetailsId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userdetails-update">

    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes,
        'role' => $role,
        'user' => $user,
        'chkrole' => $chkrole,
    ]) ?>

</div>
