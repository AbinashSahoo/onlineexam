<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserdetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userdetails-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'UserDetailsId') ?>

    <?= $form->field($model, 'Name') ?>

    <?= $form->field($model, 'Fname') ?>

    <?= $form->field($model, 'Mname') ?>

    <?= $form->field($model, 'CareOf') ?>

    <?php // echo $form->field($model, 'DOB') ?>

    <?php // echo $form->field($model, 'DOJ') ?>

    <?php // echo $form->field($model, 'DOA') ?>

    <?php // echo $form->field($model, 'Nationality') ?>

    <?php // echo $form->field($model, 'Religion') ?>

    <?php // echo $form->field($model, 'Caste') ?>

    <?php // echo $form->field($model, 'Gender') ?>

    <?php // echo $form->field($model, 'PresentAddress') ?>

    <?php // echo $form->field($model, 'PermanentAddress') ?>

    <?php // echo $form->field($model, 'Mobile') ?>

    <?php // echo $form->field($model, 'Whatsapp') ?>

    <?php // echo $form->field($model, 'Classid') ?>

    <?php // echo $form->field($model, 'BloodGroup') ?>

    <?php // echo $form->field($model, 'AdmissionNumber') ?>

    <?php // echo $form->field($model, 'ResgistrationNumber') ?>

    <?php // echo $form->field($model, 'Designation') ?>

    <?php // echo $form->field($model, 'Professional') ?>

    <?php // echo $form->field($model, 'Trained') ?>

    <?php // echo $form->field($model, 'UsertPhoto') ?>

    <?php // echo $form->field($model, 'JointPhoto') ?>

    <?php // echo $form->field($model, 'UserAdhar') ?>

    <?php // echo $form->field($model, 'Fadhar') ?>

    <?php // echo $form->field($model, 'Madhar') ?>

    <?php // echo $form->field($model, 'AdmissionForm') ?>

    <?php // echo $form->field($model, 'BirthCertificate') ?>

    <?php // echo $form->field($model, 'Qualifiaction') ?>

    <?php // echo $form->field($model, 'HighSchoolMarksheet') ?>

    <?php // echo $form->field($model, 'IntermediateMarksheet') ?>

    <?php // echo $form->field($model, 'Graduation') ?>

    <?php // echo $form->field($model, 'IsDelete') ?>

    <?php // echo $form->field($model, 'Ondate') ?>

    <?php // echo $form->field($model, 'UpdateDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
