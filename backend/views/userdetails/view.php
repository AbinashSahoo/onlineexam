<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Userdetails */

$this->title = $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Userdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<section class="content">

    <!-- Content Header (Page header) -->     
    <div class="content-header">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-md-block d-none">
                <h3 class="page-title br-0">User Details</h3>
            </div>
            <div class="w-p60">
                
            </div>
            <div class="right-title w-170">
                <span class="subheader_daterange font-weight-600">
                    <span class="subheader_daterange-label">
                        <span class="subheader_daterange-title">Today:</span>
                        <span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
                    </span>
                    <a href="#" class="btn btn-rounded btn-sm btn-primary">
                        <i class="fa fa-calendar"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>

    <div class="row">               
        
        <div class="col-12">
          <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">View Class Name </h4>
              <div class="box-controls pull-right">
                <div class="lookup lookup-circle lookup-right">
                  <input type="text" name="s">
              </div>
          </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
        <div class="table-responsive">

            <p style="float:right;margin-top:10px; margin-right:10px; ">
                <?= Html::a('Update', ['update', 'id' => $model->UserDetailsId], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->UserDetailsId], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
            // 'UserDetailsId',
                    'Name',
                    'Fname',
                    'Mname',
                    [
                        'attribute' => 'CareOf',
                        'visible' => ($model->CareOf)?true:false,
                    ],
                    'DOB',
                    [
                        'attribute' => 'DOJ',
                        'visible' => ($model->DOJ)?true:false,
                    ],
                    [
                        'attribute' => 'DOA',
                        'visible' => ($model->DOA)?true:false,
                    ],
                    'Nationality',
                    'Religion',
                    'Caste',
                    'Gender',
                    'PresentAddress',
                    'PermanentAddress',
                    'Mobile',
                    'Whatsapp',
                    [
                        'attribute' => 'Classid',
                        'visible' => ($model->Classid)?true:false,
                    ],
                    'BloodGroup',
                    [
                        'attribute' => 'AdmissionNumber',
                        'visible' => ($model->AdmissionNumber)?true:false,
                    ],
                    'ResgistrationNumber',
                    [
                        'attribute' => 'Designation',
                        'visible' => ($model->Designation)?true:false,
                    ],
                    [
                        'attribute' => 'Professional',
                        'visible' => ($model->Professional)?true:false,
                    ],
                    [
                        'attribute' => 'Trained',
                        'visible' => ($model->Trained)?true:false,
                    ],
                    [
                        'attribute'=>'UsertPhoto',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->UsertPhoto;
                        },
                        'visible' => ($model->UsertPhoto)?true:false,
                    ],
                    [
                        'attribute'=>'JointPhoto',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->JointPhoto;
                        },
                        'visible' => ($model->JointPhoto)?true:false,
                    ],
                    [
                        'attribute'=>'UserAdhar',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->UserAdhar;
                        },
                        'visible' => ($model->UserAdhar)?true:false,
                    ],
                    [
                        'attribute'=>'Fadhar',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->Fadhar;
                        },
                        'visible' => ($model->Fadhar)?true:false,
                    ],
                    [
                        'attribute'=>'Madhar',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->Madhar;
                        },
                        'visible' => ($model->Madhar)?true:false,
                    ],
                    [
                        'attribute'=>'AdmissionForm',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->AdmissionForm;
                        },
                        'visible' => ($model->AdmissionForm)?true:false,
                    ],
                    [
                        'attribute'=>'BirthCertificate',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->BirthCertificate;
                        },
                        'visible' => ($model->BirthCertificate)?true:false,
                    ],
                    
                    [
                        'attribute'=>'Qualifiaction',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->Qualifiaction;
                        },
                        'visible' => ($model->Qualifiaction)?true:false,
                    ],
                    [
                        'attribute'=>'HighSchoolMarksheet',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->HighSchoolMarksheet;
                        },
                        'visible' => ($model->HighSchoolMarksheet)?true:false,
                    ],
                    [
                        'attribute'=>'IntermediateMarksheet',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->IntermediateMarksheet;
                        },
                        'visible' => ($model->IntermediateMarksheet)?true:false,
                    ],
                    [
                        'attribute'=>'Graduation',
                        'format' => ['image',['width'=>'100','height'=>'100']],
                        'value' => function ($data)
                        {
                            return Yii::getAlias('@storageUrl')."/document/".$data->Graduation;
                        },
                        'visible' => ($model->Graduation)?true:false,
                    ],
            // 'IsDelete',
                    'Ondate',
            // 'UpdateDate',
                ],
            ]) ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>
</div>





</div>            
</section>
</div>
