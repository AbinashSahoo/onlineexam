<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Userdetails */

$this->title = 'Create Userdetails';
$this->params['breadcrumbs'][] = ['label' => 'Userdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userdetails-create">

    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes,
        'role' => $role,
        'user' => $user,
        'chkrole'=>''
    ]) ?>

</div>
