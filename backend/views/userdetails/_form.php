<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Userdetails */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="content">

    <!-- Content Header (Page header) -->     
    <div class="content-header">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-md-block d-none">
                <h3 class="page-title br-0">User</h3>
            </div>
            <div class="w-p60">

            </div>
            <div class="right-title w-170">
                <span class="subheader_daterange font-weight-600">
                    <span class="subheader_daterange-label">
                        <span class="subheader_daterange-title">Today:</span>
                        <span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
                    </span>
                    <a href="#" class="btn btn-rounded btn-sm btn-primary">
                        <i class="fa fa-calendar"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>

    <div class="row">               
        <div class="col-lg-12 col-12">
          <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Create Users</h4>
            </div>
            <!-- /.box-header -->
            <?php $form = ActiveForm::begin(['options'=>['class'=>'form']]); ?>

            <div class="box-body">
                <?php if ($model->isNewRecord) {?>
                    <?= $form->field($user, 'RoleId')->dropDownList($role, ['prompt' => 'Choose Role','onchange'=>'checkrole(this.value)'])->label('Role')?>
                <?php }else{?>
                    <?= $form->field($user, 'RoleId')->dropDownList($role, ['prompt' => 'Choose Role','onchange'=>'checkrole(this.value)'])->label('Role')?>
                <?php } ?>
                
                <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'Fname')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'Mname')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'DOB')->widget(DatePicker::classname(),['options' => ['placeholder' => 'Select date ...', 'value' => date('d-m-Y')],'pluginOptions' => ['autoclose' => true, 'format' => 'dd-mm-yyyy']]); ?>
                
                <?= $form->field($model, 'Nationality')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'Religion')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'Caste')->dropDownList(['General' => 'General', 'OBC' => 'OBC','SC' => 'SC', 'ST' => 'ST'],['prompt' => 'Choose Any...'])?>
                
                <?= $form->field($model, 'Gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female' ,'Other' => 'Other'],['prompt' => 'Choose Any..'])?>
                
                <?= $form->field($model, 'PresentAddress')->textArea(['maxlength' => true]) ?>
                
                <p>If Present & Permanent Address is same <button type="button" class="btn btn-light" onclick="copyaddresss()">copy</button></p>
                
                <?= $form->field($model, 'PermanentAddress')->textArea(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'Mobile')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'Whatsapp')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'BloodGroup')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'ResgistrationNumber')->textInput(['maxlength' => true,'required'=>true]) ?>

                <div id="sdata">

                    <?= $form->field($model, 'DOA')->widget(DatePicker::classname(),['options' => ['placeholder' => 'Select date ...'],'pluginOptions' => ['autoclose' => true, 'format' => 'dd-mm-yyyy']]); ?>
                    
                    <?= $form->field($model, 'Classid')->dropDownList($classes, ['prompt' => 'Choose Class along with secrtion'])?>
                    
                    <?= $form->field($model, 'AdmissionNumber')->textInput(['maxlength' => true]) ?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'JointPhoto')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->JointPhoto,['width'=>'100px'])?>
                        <?= $form->field($model, 'JointPhoto')->fileInput() ?>
                        <input type="hidden" name="oldJphoto" value="<?=$model->JointPhoto?>">
                    <?php } ?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'Fadhar')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->Fadhar,['width'=>'100px'])?>
                        <?= $form->field($model, 'Fadhar')->fileInput() ?>
                        <input type="hidden" name="oldFadhar" value="<?=$model->Fadhar?>">
                    <?php } ?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'Madhar')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->Madhar,['width'=>'100px'])?>
                        <?= $form->field($model, 'Madhar')->fileInput() ?>
                        <input type="hidden" name="oldMadhar" value="<?=$model->Madhar?>">
                    <?php } ?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'AdmissionForm')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->AdmissionForm,['width'=>'100px'])?>
                        <?= $form->field($model, 'AdmissionForm')->fileInput() ?>
                        <input type="hidden" name="oldAform" value="<?=$model->AdmissionForm?>">
                    <?php } ?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'BirthCertificate')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->BirthCertificate,['width'=>'100px'])?>
                        <?= $form->field($model, 'BirthCertificate')->fileInput() ?>
                        <input type="hidden" name="oldBirth" value="<?=$model->BirthCertificate?>">
                    <?php } ?>
                </div>

                <?php if ($model->isNewRecord) {?>
                    <?= $form->field($model, 'UsertPhoto')->fileInput() ?>
                <?php }else{?>
                    <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->UsertPhoto,['width'=>'100px'])?>
                    <?= $form->field($model, 'UsertPhoto')->fileInput() ?>
                    <input type="hidden" name="oldUphoto" value="<?=$model->UsertPhoto?>">
                <?php } ?>
                
                <?php if ($model->isNewRecord) {?>
                    <?= $form->field($model, 'UserAdhar')->fileInput() ?>
                <?php }else{?>
                    <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->UserAdhar,['width'=>'100px'])?>
                    <?= $form->field($model, 'UserAdhar')->fileInput() ?>
                    <input type="hidden" name="oldUadhar" value="<?=$model->UserAdhar?>">
                <?php } ?>

                <div id="tdata">

                    <?= $form->field($model, 'DOJ')->widget(DatePicker::classname(),['options' => ['placeholder' => 'Select date ...'],'pluginOptions' => ['autoclose' => true, 'format' => 'dd-mm-yyyy']]); ?>
                    
                    <?= $form->field($model, 'CareOf')->textInput(['maxlength' => true]) ?>
                    
                    <?= $form->field($model, 'Designation')->textInput(['maxlength' => true]) ?>
                    
                    <?= $form->field($model, 'Professional')->dropDownList(['1' => 'Yes', '0' => 'No'], ['prompt' => 'Choose Any...'])?>
                    
                    <?= $form->field($model, 'Trained')->dropDownList(['1' => 'Yes', '0' => 'No'], ['prompt' => 'Choose Any...'])?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'Qualifiaction')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->Qualifiaction,['width'=>'100px'])?>
                        <?= $form->field($model, 'Qualifiaction')->fileInput() ?>
                        <input type="hidden" name="oldQual" value="<?=$model->Qualifiaction?>">
                    <?php } ?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'HighSchoolMarksheet')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->HighSchoolMarksheet,['width'=>'100px'])?>
                        <?= $form->field($model, 'HighSchoolMarksheet')->fileInput() ?>
                        <input type="hidden" name="oldHsM" value="<?=$model->HighSchoolMarksheet?>">
                    <?php } ?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'IntermediateMarksheet')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->IntermediateMarksheet,['width'=>'100px'])?>
                        <?= $form->field($model, 'IntermediateMarksheet')->fileInput() ?>
                        <input type="hidden" name="oldIntM" value="<?=$model->IntermediateMarksheet?>">
                    <?php } ?>
                    
                    <?php if ($model->isNewRecord) {?>
                        <?= $form->field($model, 'Graduation')->fileInput() ?>
                    <?php }else{?>
                        <?= Html::img(Yii::getAlias('@storageUrl')."/document/".$model->Graduation,['width'=>'100px'])?>
                        <?= $form->field($model, 'Graduation')->fileInput() ?>
                        <input type="hidden" name="oldGrad" value="<?=$model->Graduation?>">
                    <?php } ?>
                </div>
                <?php if ($model->isNewRecord) {?>
                    <?= $form->field($user, 'Password')->textInput(['maxlength' => true]) ?>
                <?php }else{?>
                    <?= $form->field($user, 'Password')->textInput(['maxlength' => true]) ?>
                    <?php } ?>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <?= Html::submitButton('<i class="ti-save-alt"></i> Save', ['class' => 'btn btn-primary']) ?>
            </div>  
            <?php ActiveForm::end(); ?>
        </div>
        <!-- /.box -->            
    </div>
</div>            
</section>
<script type="text/javascript">
    <?php
    if ($chkrole != '') {?>
        setTimeout(function() {
          checkrole(<?=$chkrole?>);
      }, 1000);
    <?php }
    ?>
    function checkrole(value)
    {
        if (value == 3) {
            $('#sdata').css('display','block');
            $('#tdata').css('display','none');
        }else{
            $('#sdata').css('display','none');
            $('#tdata').css('display','block');
        }
    }

    function copyaddresss()
    {
        $('#userdetails-permanentaddress').val($('#userdetails-presentaddress').val());
    }
</script>