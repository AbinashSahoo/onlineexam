<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Subject */

$this->title = $model->SubjectId;
$this->params['breadcrumbs'][] = ['label' => 'Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Subject</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<div class="row">				
				
				<div class="col-12">
				  <div class="box">
					<div class="box-header with-border">
					  <h4 class="box-title">View Subject Name </h4>
					  <div class="box-controls pull-right">
						<div class="lookup lookup-circle lookup-right">
						  <input type="text" name="s">
						</div>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="table-responsive">

   
    <p style="float:right;margin-top:10px; margin-right:10px; ">
        <?= Html::a('Update', ['update', 'id' => $model->SubjectId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->SubjectId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'SubjectId',
            [
                'attribute' => 'Classid',
                'value' => function ($data) {
                  if ($data->classes) {
                    return $data->classes->ClassName;
                  }
                },
            ],
            'SubjectName',
            // 'SubFullMark',
            // 'IsDelete',
            'Ondate',
            // 'UpdateDate',
        ],
    ]) ?>

</div>
					</div>
					<!-- /.box-body -->
				  </div>
				</div>
				
				
				
				
				
			  </div>			
		</section>
</div>
