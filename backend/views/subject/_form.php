<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Subject */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="content">

	<!-- Content Header (Page header) -->	  
	<div class="content-header">
		<div class="d-flex align-items-center justify-content-between">
			<div class="d-md-block d-none">
				<h3 class="page-title br-0">Subject</h3>
			</div>
			<div class="w-p60">

			</div>
			<div class="right-title w-170">
				<span class="subheader_daterange font-weight-600">
					<span class="subheader_daterange-label">
						<span class="subheader_daterange-title">Today:</span>
						<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
					</span>
					<a href="#" class="btn btn-rounded btn-sm btn-primary">
						<i class="fa fa-calendar"></i>
					</a>
				</span>
			</div>
		</div>
	</div>

	<div class="row">				


		<div class="col-lg-6 col-12">
			<div class="box">
				<div class="box-header with-border">
					<h4 class="box-title">Create Subject</h4>
				</div>
				<!-- /.box-header -->
				<?php $form = ActiveForm::begin(['options'=>['class'=>'form']]); ?>
				<div class="box-body">

					<?= $form->field($model, 'Classid')->dropDownList($classes, ['prompt' => 'Choose Class along with secrtion','onchange'=>'subjectlist(this.value)'])?>

					<?= $form->field($model, 'SubjectName')->textInput(['maxlength' => true,'placeholder'=>'Subject Name']) ?>

					<?= $form->field($model, 'SubFullMark')->hiddenInput(['maxlength' => true,'placeholder'=>'Subject FullMark'])->label(false) ?>

				</div>
				<!-- /.box-body -->
				<div class="box-footer">

					<?= Html::submitButton('<i class="ti-save-alt"></i> Save', ['class' => 'btn btn-primary']) ?>
				</div>  
				<?php ActiveForm::end(); ?>
			</div>
			<!-- /.box -->			
		</div>
		<div class="col-lg-6 col-12">
			<div class="box">
				<div class="box-header with-border">
					<h4 class="box-title">Existing Subject list for this class</h4>
				</div>
				<div class="box-body">
					<ul id="subdata">
						
					</ul>

				</div>
			</div>
			<!-- /.box -->			
		</div>
	</div>			
</section>
<script>
	function subjectlist(value)
	{
    $.ajax({url:"<?=Url::toRoute(['question/subjectlist'])?>?classid="+value,
            success:function(results)
            {
                if(results)
                {
                	$('#subdata').html('');
                    var subject=JSON.parse(results);
                    $.each(subject,function(key,value){
                        $('#subdata').append('<li>'+value.SubjectName+'</li>');
                    });
                }else{
                	$('#subdata').html('<li>Norecord found...</li>');
                }
            }
        });
	}
</script>

