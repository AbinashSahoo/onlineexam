<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Notice */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content">

    <!-- Content Header (Page header) -->     
    <div class="content-header">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-md-block d-none">
                <h3 class="page-title br-0">Notice</h3>
            </div>
            <div class="w-p60">

            </div>
            <div class="right-title w-170">
                <span class="subheader_daterange font-weight-600">
                    <span class="subheader_daterange-label">
                        <span class="subheader_daterange-title">Today:</span>
                        <span class="subheader_daterange-date text-primary"><?=date('M d')?></span>
                    </span>
                    <a href="#" class="btn btn-rounded btn-sm btn-primary">
                        <i class="fa fa-calendar"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div class="row">               
        <div class="col-lg-6 col-12">
          <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">Create Notice</h4>
            </div>
            <!-- /.box-header -->
                <?php $form = ActiveForm::begin(['options'=>['class'=>'form']]); ?>
                <div class="box-body">
                    <?= $form->field($model, 'ClassId')->dropDownList($classes,['prompt' => 'Choose Class...'])?>

                    <?= $form->field($model, 'Title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'Description')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'EndDate')->widget(DatePicker::classname(),['options' => ['placeholder' => 'Select date ...', 'value' => date('d-m-Y')],'pluginOptions' => ['autoclose' => true, 'format' => 'dd-mm-yyyy']]); ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                   <?= Html::submitButton('<i class="ti-save-alt"></i> Save', ['class' => 'btn btn-primary']) ?>
                </div>  
                <?php ActiveForm::end(); ?>
            </div>
            <!-- /.box -->            
        </div>    
    </div>            
</section>
<?php

   $this->registerJs(
    "tinymce.init({
      selector: '#notice-description'
    });"
    );
?>