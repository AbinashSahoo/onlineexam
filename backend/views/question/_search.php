<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\QuestionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'QuestionId') ?>

    <?= $form->field($model, 'Questions') ?>

    <?= $form->field($model, 'Ans_a') ?>

    <?= $form->field($model, 'Ans_b') ?>

    <?= $form->field($model, 'Ans_c') ?>

    <?php // echo $form->field($model, 'Ans_d') ?>

    <?php // echo $form->field($model, 'CorrectAns') ?>

    <?php // echo $form->field($model, 'QuestionMark') ?>

    <?php // echo $form->field($model, 'QuestionSetId') ?>

    <?php // echo $form->field($model, 'IsDelete') ?>

    <?php // echo $form->field($model, 'Ondate') ?>

    <?php // echo $form->field($model, 'UpdateDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
