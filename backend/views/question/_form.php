<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Question */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Questions</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<div class="row">				
				
				
				<div class="col-lg-6 col-12">
					  <div class="box">
						<div class="box-header with-border">
						  <h4 class="box-title">Create Questions</h4>
						</div>
						<!-- /.box-header -->
						 <?php $form = ActiveForm::begin(['options'=>['class'=>'form']]); ?>
						 <div class="box-body">

    <?= $form->field($model, 'ClassId')->dropDownList($classes,['prompt' => 'Choose Class...','onchange'=>'subjectlist(this.value)'])?>

    <?= $form->field($model, 'SubjectId')->dropDownList(['prompt' => 'Choose Subject...'],['onchange'=>'chapterlist(this.value)'])?>

    <?= $form->field($model, 'ChapterId')->dropDownList(['prompt' => 'Choose Chapter...'])?>

    <?= $form->field($model, 'Questions')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ans_a')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ans_b')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ans_c')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ans_d')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CorrectAns')->dropDownList(['' => 'Choose Correct Answer', 'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D']) ?>

    <?= $form->field($model, 'QuestionMark')->textInput() ?>

   </div>
							<!-- /.box-body -->
							<div class="box-footer">
								
								 <?= Html::submitButton('<i class="ti-save-alt"></i> Save', ['class' => 'btn btn-primary']) ?>
							</div>  
						 <?php ActiveForm::end(); ?>
					  </div>
					  <!-- /.box -->			
				</div>
				
				
				
				
				
			  </div>			
		</section>
<script type="text/javascript">
<?php
    if ($model->ClassId != '') {?>
        setTimeout(function() {
      subjectlist(<?=$model->ClassId?>);
    }, 1000);
   <?php }
?>
function subjectlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['question/subjectlist'])?>?classid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#question-subjectid').html('<option value="">Select Subject</option>');
                    var subject=JSON.parse(results);
                    $.each(subject,function(key,value){
                        $('#question-subjectid').append('<option value="'+value.SubjectId+'">'+value.SubjectName+'</option>');
                    });
                    <?php
                      if ($model->ClassId != '') {
                      ?>
                        $("#question-subjectid").val(<?= $model->SubjectId; ?>);
                        var key = $('#question-subjectid option:selected').val();
                        chapterlist(key);

                      <?php
                      }
                      ?>
                    
                }
            }
        });
}

function chapterlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['question/chapterlist'])?>?subid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#question-chapterid').html('<option value="">Select Chapter</option>');
                    var chapter=JSON.parse(results);
                    $.each(chapter,function(key,value){
                        $('#question-chapterid').append('<option value="'+value.ChapterId+'">'+value.ChapterName+'</option>');
                    });
                    <?php
                      if ($model->ClassId != '') {
                      ?>
                        $("#question-chapterid").val(<?= $model->ChapterId; ?>);

                      <?php
                      }
                      ?>
                    
                }
            }
        });
}
</script>