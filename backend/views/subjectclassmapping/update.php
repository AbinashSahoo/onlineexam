<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Subjectclassmapping */

$this->title = 'Update Subjectclassmapping: ' . $model->SubjectClassId;
$this->params['breadcrumbs'][] = ['label' => 'Subjectclassmappings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SubjectClassId, 'url' => ['view', 'id' => $model->SubjectClassId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subjectclassmapping-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'teacherid' => $teacherid,
        'classes' => $classes,
    ]) ?>

</div>
