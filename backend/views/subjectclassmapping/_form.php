<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Subjectclassmapping */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Subjectclassmapping</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<div class="row">				
				
				
				<div class="col-lg-6 col-12">
					  <div class="box">
						<div class="box-header with-border">
						  <h4 class="box-title">Create Subjectclassmapping</h4>
						</div>
						<!-- /.box-header -->
						 <?php $form = ActiveForm::begin(['options'=>['class'=>'form']]); ?>

						
							<div class="box-body">

    <?= $form->field($model, 'TeacherId')->dropDownList($teacherid, ['prompt' => 'Choose Teacher...'])?>

    <?= $form->field($model, 'ClassId')->dropDownList($classes, ['prompt' => 'Choose Class...','onchange'=>'subjectlist(this.value)'])?>

    <?= $form->field($model, 'SubjetId')->dropDownList(['prompt' => 'Choose Subjet...'])?>
<!-- 
    <?= $form->field($model, 'ChapterId')->dropDownList(['prompt' => 'Choose Chapter...'])?> -->

    </div>
							<!-- /.box-body -->
							<div class="box-footer">
								
								 <?= Html::submitButton('<i class="ti-save-alt"></i> Save', ['class' => 'btn btn-primary']) ?>
							</div>  
						 <?php ActiveForm::end(); ?>
					  </div>
					  <!-- /.box -->			
				</div>
				
				
				
				
				
			  </div>			
		</section>
<script type="text/javascript">
<?php
    if ($model->ClassId != '') {?>
        setTimeout(function() {
      subjectlist(<?=$model->ClassId?>);
    }, 1000);
   <?php }
?>
function subjectlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['subjectclassmapping/subjectlist'])?>?classid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#subjectclassmapping-subjectid').html('<option value="">Select Subject</option>');
                    var subject=JSON.parse(results);
                    $.each(subject,function(key,value){
                        $('#subjectclassmapping-subjetid').append('<option value="'+value.SubjectId+'">'+value.SubjectName+'</option>');
                    });
                    <?php
                      if ($model->ClassId != '') {
                      ?>
                        $("#subjectclassmapping-subjetid").val(<?= $model->SubjetId; ?>);
                        var key = $('#subjectclassmapping-subjetid option:selected').val();
                        chapterlist(key);

                      <?php
                      }
                      ?>
                    
                }
            }
        });
}

// function chapterlist(value)
// {
//     $.ajax({url:"<?=Url::toRoute(['subjectclassmapping/chapterlist'])?>?subid="+value,
//             success:function(results)
//             {
//                 if(results)
//                 {
//                     $('#subjectclassmapping-chapterid').html('<option value="">Select Chapter</option>');
//                     var chapter=JSON.parse(results);
//                     $.each(chapter,function(key,value){
//                         $('#subjectclassmapping-chapterid').append('<option value="'+value.ChapterId+'">'+value.ChapterName+'</option>');
//                     });
                    
//                 }
//             }
//         });
// }
</script>