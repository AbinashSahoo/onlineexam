<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SubjectclassmappingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subjectclassmapping-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'SubjectClassId') ?>

    <?= $form->field($model, 'ClassId') ?>

    <?= $form->field($model, 'TeacherId') ?>

    <?= $form->field($model, 'SubjetId') ?>

    <?= $form->field($model, 'IsDelete') ?>

    <?php // echo $form->field($model, 'Ondate') ?>

    <?php // echo $form->field($model, 'UpdateDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
