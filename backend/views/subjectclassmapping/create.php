<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Subjectclassmapping */

$this->title = 'Create Subjectclassmapping';
$this->params['breadcrumbs'][] = ['label' => 'Subjectclassmappings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subjectclassmapping-create">

   
    <?= $this->render('_form', [
        'model' => $model,
        'teacherid' => $teacherid,
        'classes' => $classes,
    ]) ?>

</div>
