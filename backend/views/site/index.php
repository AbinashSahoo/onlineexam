<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

  
		
		<!-- Main content -->
		<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Dashboard</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary">Jul 22</span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<div class="row">				
				
				<div class="col-xl-3 col-lg-6 col-12">
					<div class="box bg-info">
						<div class="box-body">
							<div class="d-flex">
								<h3 class="font-weight-600 mb-0">8,958</h3>
								<span class="badge badge-danger badge-pill align-self-center ml-auto">+22,6%</span>
							</div>
							<div class="boxcontent">
								Students Registered
								<div class="font-size-16">854 avg</div>
							</div>
						</div>
						
					</div>
				</div>			
				<div class="col-xl-3 col-lg-6 col-12">
					<div class="box bg-info">
						<div class="box-body">
							<div class="d-flex">
								<h3 class="font-weight-600 mb-0">9,854</h3>
								<span class="badge badge-danger badge-pill align-self-center ml-auto">+22,6%</span>
							</div>
							<div class="boxcontent">
								Students Given Exam
								<div class="font-size-16">6,854 avg</div>
							</div>
						</div>
						
					</div>
				</div>			
				<div class="col-xl-3 col-lg-6 col-12">
					<div class="box bg-info">
						<div class="box-body">
							<div class="d-flex">
								<h3 class="font-weight-600 mb-0">3,952</h3>
							</div>
							<div class="boxcontent">
								Passed Students
								<div class="font-size-16">9,758 avg</div>
							</div>
						</div>
						
					</div>
				</div>			
				<div class="col-xl-3 col-lg-6 col-12">
					<div class="box bg-info">
						<div class="box-body">
							<div class="d-flex">
								<h3 class="font-weight-600 mb-0">20%</h3>
								<div class="list-icons ml-auto">
									<div class="list-icons-item dropdown">
										<a href="#" class="list-icons-item dropdown-toggle text-white" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
										<div class="dropdown-menu dropdown-menu-right">
											<a href="#" class="dropdown-item">Update data</a>
											<a href="#" class="dropdown-item">Detailed log</a>
											<a href="#" class="dropdown-item">Statistics</a>
											<a href="#" class="dropdown-item">Clear list</a>
										</div>
									</div>
								</div>
							</div>
							<div class="boxcontent">
								Failed Students
								<div class="font-size-16">85.6% avg</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-12 col-xl-5">
				  <div class="box" >
					<div class="box-header with-border">
					  <h4 class="box-title">Question Bank Subject Wise</h4>
					</div>
					<div class="box-body">
					  <div id="flotPie2" class="h-300"></div>
					</div>
					<!-- /.box-body-->
				  </div>
				  <!-- /.box -->
				</div>
				<div class="col-12 col-xl-7">
				  <div class="box" >
					<div class="box-header with-border">
					  <h4 class="box-title">Recent Exam Results</h4>
					</div>
					<div class="box-body">
					  <div id="bookingstatus"></div>
					</div>
				  </div>
				</div>
				
				<div class="col-12 col-xl-6">
				  <div class="box" style="height:380px;">
					<div class="box-header with-border">
					  <h4 class="box-title">Student Statistic Table </h4>
					  <div class="box-controls pull-right">
						<div class="lookup lookup-circle lookup-right">
						  <input type="text" name="s">
						</div>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="table-responsive">
						  <table class="table table-hover">
							<tbody><tr>
							  <th>Class Name</th>
							  <th>Total Students</th>
							  <th>Total Enrolled</th>
							  <th>Pending</th>
							  <th>Status</th>
							  <th>Total Suspended</th>
							</tr>
							<tr>
							  <td><a href="javascript:void(0)">Std-VI</a></td>
							  <td>100</td>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> 80</span> </td>
							  <td>20</td>
							  <td><span class="badge badge-pill badge-danger">Pending</span></td>
							  <td>10</td>
							</tr>
							<tr>
							  <td><a href="javascript:void(0)">Std-VII</a></td>
							  <td>100</td>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> 80</span> </td>
							  <td>20</td>
							  <td><span class="badge badge-pill badge-danger">Pending</span></td>
							  <td>10</td>
							</tr>
							<tr>
							  <td><a href="javascript:void(0)">Std-IX</a></td>
							  <td>120</td>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> 100</span> </td>
							  <td>20</td>
							  <td><span class="badge badge-pill badge-warning">Shipped</span></td>
							  <td>5</td>
							</tr>
							<tr>
							  <td><a href="javascript:void(0)">Std-VII</a></td>
							  <td>150</td>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> 110</span> </td>
							  <td>40</td>
							  <td><span class="badge badge-pill badge-danger">Prossing</span></td>
							  <td>5</td>
							</tr>
							
						  </tbody></table>
						</div>
					</div>
					<!-- /.box-body -->
				  </div>
				</div>
				
				<div class="col-12 col-xl-6">
					<div class="box" style="height:380px;">
					<div class="box-header with-border">
					  <h4 class="box-title">Upcoming Exams </h4>
					  <div class="box-controls pull-right">
						<div class="lookup lookup-circle lookup-right">
						  <input type="text" name="s">
						</div>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="table-responsive">
						  <table class="table table-hover">
							<tbody><tr>
							  <th>Date</th>
							  <th>Exam Name</th>
							  <th>Class Name</th>
							  <th>Marks</th>
							  
							</tr>
							<tr>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 16, 2017</span> </td>
							  <td>Exam1</a></td>
							  <td>Std-V</td>
							  <td>100</td>
							  
							</tr>
							<tr>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 16, 2017</span> </td>
							  <td>Exam1</a></td>
							  <td>Std-V</td>
							  <td>100</td>
							  
							</tr>
							<tr>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 16, 2017</span> </td>
							  <td>Exam1</a></td>
							  <td>Std-V</td>
							  <td>100</td>
							 
							</tr>
							<tr>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 16, 2017</span> </td>
							  <td>Exam1</a></td>
							  <td>Std-V</td>
							  <td>100</td>
							 
							</tr>
							<tr>
							  <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 16, 2017</span> </td>
							  <td>Exam1</a></td>
							  <td>Std-V</td>
							  <td>100</td>
							  
							</tr>
							
						  </tbody></table>
						</div>
					</div>
				</div>
				
			  </div>			
		</section>
		<!-- /.content -->
	 