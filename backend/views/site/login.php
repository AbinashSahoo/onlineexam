<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<style>
.form-group{width:88%;}
.form-control{border-top-left-radius:0px; border-bottom-left-radius:0px; border-left:none;}
</style>

<div class="site-login" style="">

    <div class="h-p100">
        <div class="row align-items-center justify-content-md-center h-p100">
                <div class="col-lg-6 col-12 h-lg-p100 h-auto bg-img" style="">
                    <div class="row l-block  p-lg-50  h-lg-p100 h-auto" style="padding-bottom:0px;">
                        <div class="col-12 logo align-self-center">
                            <a href="#" class="aut-logo">
                                <img src="../images/logo-light.png" alt="" style="width:80px; height:80px; float:left; margin-right:20px;">
                                <h1 class="title" >Welcome to K-Test</h1>
                                <h3 class="subtitle ">Your ultimate destination for online assessment</h3>
                            </a>
                        </div>
                        <div class="col-12 align-self-center">

                        </div>
                        <div class="col-12 align-self-end">
                            <h6 class="text-white-50">
                                © 2020 K-Test 
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12" style="margin-top:200px;">
                        <div class="row justify-content-center no-gutters">
                            <div class="col-xl-6 col-lg-7 col-md-6 col-12">
                                <div class="content-top-agile p-10">
                                    <h2 class="text-primary">Get started with Us</h2>
                                    <p class="text-black-50">Sign in to start your session</p>                          
                                </div>
                                <div class="p-30 rounded10 b-2 b-dashed border-info">
                                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                                    <div class="form-group" style="display:none;">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
                                            </div>
                                            <select class="form-control pl-15 bg-transparent plc-black">
                                                <option>Login as</option>
                                                <option>Admin</option>
                                                <option>Teacher</option>
                                                <option>Student</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
                                            </div>

                                            <?= $form->field($model, 'UserName')->textInput(['autofocus' => true,'class' => 'form-control pl-15 bg-transparent plc-black','placeholder'=>'UserName'])->label(false) ?>
                                            
                                        </div>
                                    
                                   
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
                                            </div>

                                           
                                             <?= $form->field($model, 'Password')->passwordInput(['autofocus' => true,'class' => 'form-control pl-15 bg-transparent plc-black','placeholder'=>'Password'])->label(false) ?>
                                        </div>
                                    
                                    <div class="row">
                                        <div class="col-6" style="display:none;">
                                          <div class="checkbox">

                                            <?= $form->field($model, 'rememberMe')->checkbox() ?>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-6" style="display:none;">
                                       <div class="fog-pwd text-right">
                                        <a href="javascript:void(0)" class="hover-info"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <!-- /.col -->
                                <div class="col-12 text-center">
                                   <?= Html::submitButton('Login', ['class' => 'btn btn-info mt-10', 'name' => 'login-button']) ?>
                               </div>
                               <!-- /.col -->
                                </div>

                        <!-- <div class="form-group">
                            
                        </div> -->

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
