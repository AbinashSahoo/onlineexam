<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Questionset */

$this->title = 'Questions View';
$this->params['breadcrumbs'][] = ['label' => 'Questionsets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="questionset-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <table>
        <th> Set <?=$SetName?></th>
        <th>Action</th>
        <tbody>
        <?php
        $rid = 1;
        foreach ($model as $key => $value) {?>
            <tr id="<?=$rid?>">
                <td><?=$value->question->Questions?></td>
                <td><a href="#" onclick= "remove('<?=$value->questionsetId?>')">Remove</a></td>
                <td><a href="<?=Url::toRoute(['question/update','id'=>$value->QuestionId])?>" target="_blank" >Edit</a></td>
            </tr>
       <?php $rid++;}
        ?>
        </tbody>
    </table>
    <button class ='btn btn-primary'>Addmore Question</button>

</div>
<script>
function remove(qsetid) {
    $.ajax({url:"<?=Url::toRoute(['questionset/delete'])?>?id="+qsetid,
        success:function(results)
        {
            if(results){
                alert("Question is removed");
                window.location.reload();
            }else{
                alert("Something went wrong!");
            }
            
        }
    });
}
</script>
