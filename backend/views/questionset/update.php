<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Questionset */

$this->title = 'Update Questionset: ' . $model->questionsetId;
$this->params['breadcrumbs'][] = ['label' => 'Questionsets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->questionsetId, 'url' => ['view', 'id' => $model->questionsetId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="questionset-update">


    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes,

    ]) ?>

</div>
