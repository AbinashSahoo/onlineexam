<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\QuestionsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Questionsets';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Questionset</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

   <div class="row">				
				
				
				<div class="col-lg-12 col-12">
					  <div class="box">
						<div class="box-header with-border">
						  <h4 class="box-title">Search Questionset</h4>
						</div>
						<!-- /.box-header -->
						 <?php $form = ActiveForm::begin(['options'=>['class'=>'form']]); ?>

						
							<div class="box-body">
								<div class="row">
								<div class="col-lg-3 col-3">
									<label>Class</label>
									<select id="class-id" name="ClassId" class="form-control" required onchange="subjectlist(this.value);">
										<option value="">Choose Class</option>
										<?php
										if ($classes) {
										  foreach ($classes as $ck => $cval) {
										?>
											<option value="<?= $cval->ClassId; ?>" <?php if ($classid == $cval->ClassId) echo "selected"; ?>><?= $cval->ClassName ?></option>
										<?php
										  }
										}
										?>
									</select>
								</div>
								<div class="col-lg-3 col-3">
									<label>Subject</label>
									<select id="subject-id" name="SubjectId" class="form-control" required onchange="chapterlist(this.value);">
										<option value="">Choose Subject</option>
									</select>
								</div>
								<div class="col-lg-3 col-3">
									<label>Chapter</label>
									<select id="chapter-id" name="ChapterId" class="form-control" required>
										<option value="">Choose Chapter</option>
									</select>
								</div>
								<div class="col-lg-3 col-3">
									<br/>
									 <?= Html::submitButton('<i class="ti-maginfy"></i> Search', ['class' => 'btn btn-primary','name'=>'search']) ?>
								</div>
								</div>
								
							</div>
							<!-- /.box-body -->
							
						 <?php ActiveForm::end(); ?>
					  </div>
					  <!-- /.box -->			
				</div>
				
			</div>	
<div class="row">				
				
				<div class="col-12">
				  <div class="box">
					<div class="box-header with-border">
					  <h4 class="box-title">View Questionset Name </h4>
					  <div class="box-controls pull-right">
						<div class="lookup lookup-circle lookup-right">
						  <input type="text" name="s">
						</div>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="table-responsive">				
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'tableOptions' => ['class' => 'table table-hover table-striped'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'questionsetId',
            [
                'attribute' => 'ClassId',
                'value' => function ($data) {
                  if ($data->classes) {
                    return $data->classes->ClassName;
                  }
                },
            ],
            [
                'attribute' => 'SubjetId',
                'value' => function ($data) {
                  if ($data->subject) {
                    return $data->subject->SubjectName;
                  }
                },
            ],
            // [
            //     'attribute' => 'ChapterId',
            //     'value' => function ($data) {
            //       if ($data->chapter) {
            //         return $data->chapter->ChapterName;
            //       }
            //     },
            // ],
            'SetName',
            [
                'attribute' => 'Questions',
                'format' => 'html',
                'value' => function ($data) {
                    $number = $data->getNoqsn($data->ClassId,$data->SubjetId,$data->SetName);
                    $link= '';
                    if($number){
                        $link .= '<a href="'.Url::toRoute(['viewqsns','classid'=>$data->ClassId,'subjectid'=>$data->SubjetId,'setname'=>$data->SetName]).'">'.$number.'</a>';
                    }else{
                        $link = $number;
                    }
                    return $link;
                },
            ],
            // [
            //     'attribute' => 'QuestionId',
            //     'value' => function ($data) {
            //       if ($data->question) {
            //         return $data->question->Questions;
            //       }
            //     },
            // ],
            [
                'header'=>'Enable Exam',
                'value'=> function($data)
                          {
                            if($data->examenable){
                                return 'already started';
                            }else{
                                return '<a id = "enb-btn" href="#" onclick="start('.$data->ClassId.','.$data->SubjetId.',\''.$data->SetName.'\')"><input type="button" value="Start" class="btn btn-success" /></a>';
                            }
                                    
                          },
                 'format' => 'raw'
           ],
            //'IsDelete',
            'Ondate',
            //'UpdateDate',

            ['class' => 'yii\grid\ActionColumn','template' => '{delete}'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
					</div>
					<!-- /.box-body -->
				  </div>
				</div>
				
				
				
				
				
			  </div>			
		</section>


<script type="text/javascript">
<?php
if ($classid != '') {?>
    setTimeout(function() {
  subjectlist(<?=$classid?>);
}, 1000);
<?php }
?>
function subjectlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['question/subjectlist'])?>?classid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#subject-id').html('<option value="">Select Subject</option>');
                    var subject=JSON.parse(results);
                    $.each(subject,function(key,value){
                        $('#subject-id').append('<option value="'+value.SubjectId+'">'+value.SubjectName+'</option>');
                    });

                    <?php
                      if ($classid != '') {
                      ?>
                        $("#subject-id").val(<?=$subjectid?>);
                        var key = $('#subject-id option:selected').val();
                        chapterlist(key);

                      <?php
                      }
                      ?>
                    
                }
            }
        });
}

function chapterlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['question/chapterlist'])?>?subid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#chapter-id').html('<option value="">Select Chapter</option>');
                    var chapter=JSON.parse(results);
                    $.each(chapter,function(key,value){
                        $('#chapter-id').append('<option value="'+value.ChapterId+'">'+value.ChapterName+'</option>');
                    });
                    <?php
                      if ($classid != '') {
                      ?>
                        $("#chapter-id").val(<?=$chapterid?>);

                      <?php
                      }
                      ?>
                    
                }
            }
        });
}
function start(classid,subid,setname)
{
  var duration = prompt("Please enter exam duration:", "3");
  if (duration == null || duration == "" ) {
    alert("Are you sure You won't start");
  } else {
    $.ajax({url:"<?=Url::toRoute(['questionset/examstart'])?>?classid="+classid+"& setname="+setname+"&duration="+duration+"&subid="+subid,
            success:function(results)
            {
                if(results == 1)
                {
                    alert("Exam is started ");
                    window.location.reload();
                }
                else{
                    alert("Something went wrong!");
                }
            }
        });
  }
}
</script>