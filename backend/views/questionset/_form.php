<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Questionset */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content">
			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Questionset</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>
			<div class="row">					
				<div class="col-lg-12 col-12">
					  <div class="box">
						<div class="box-header with-border">
						  <h4 class="box-title">Create Questionset</h4>
						</div>
						<!-- /.box-header -->
						 <?php $form = ActiveForm::begin(['options'=>['class'=>'form']]); ?>
							<div class="box-body">

                            <?= $form->field($model, 'ClassId')->dropDownList($classes,['prompt' => 'Choose Class...','onchange'=>'subjectlist(this.value)'])?>

                            <?= $form->field($model, 'SubjetId')->dropDownList(['prompt' => 'Choose Subject...'],['onchange'=>'chapterlist(this.value)'])?>

                            <?= $form->field($model, 'ChapterId')->dropDownList(['prompt' => 'Choose chapter...'])?>

                            <?= $form->field($model, 'SetName')->textInput(['maxlength' => true]) ?>

                            </div>
							<!-- /.box-body -->
							<div class="box-footer">
								
								 <?= Html::submitButton('<i class="ti-save-alt"></i> Question', ['class' => 'btn btn-primary','name'=>'question']) ?>
							</div>  
						    <?php ActiveForm::end(); ?>
                                <div class="box-body" style="display: <?=($questions != '')?'block':'none'?>">

                                <?php $form = ActiveForm::begin(); ?>

                                <?= $form->field($model, 'ClassId')->hiddenInput(['value'=> $classid])->label(false)?>

                                <?= $form->field($model, 'SubjetId')->hiddenInput(['value'=> $subjectid])->label(false)?>

                                <?= $form->field($model, 'ChapterId')->hiddenInput(['value'=> $chapterid])->label(false)?>

                                <?= $form->field($model, 'SetName')->hiddenInput(['value'=> $setname])->label(false) ?>

                                <label class="control-label">Questions</label><br>
                                <?php
                                if($questions){
                                    $a=1;
                                foreach ($questions as $key => $value) {?>
                                    <input type="checkbox" id="QuestionId<?=$a?>" name="QuestionId[]" value="<?=$value->QuestionId?>">
                                    <label for="QuestionId<?=$a?>"> <?=$value->Questions?></label><br>
                                <?php $a++; }}
                                ?>
                                <div class="box-footer">
                                
                                 <?= Html::submitButton('<i class="ti-save-alt"></i> Save', ['class' => 'btn btn-primary','name'=>'save']) ?>
                                </div>
                            <?php ActiveForm::end(); ?>

                        </div>
					  </div>
					  <!-- /.box -->			
				</div>			
			</div>			
		</section>


<script type="text/javascript">
<?php
if ($classid != '') {?>
    setTimeout(function() {
        $("#questionset-classid").val(<?=$subjectid?>);
        $("#questionset-setname").val('<?=$setname?>');
  subjectlist(<?=$classid?>);
}, 1000);
<?php }
?>
function subjectlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['question/subjectlist'])?>?classid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#questionset-subjetid').html('<option value="">Select Subject</option>');
                    var subject=JSON.parse(results);
                    $.each(subject,function(key,value){
                        $('#questionset-subjetid').append('<option value="'+value.SubjectId+'">'+value.SubjectName+'</option>');
                    });
                   <?php
                      if ($classid != '') {
                      ?>
                        $("#questionset-subjetid").val(<?=$subjectid?>);
                        var key = $('#questionset-subjetid option:selected').val();
                        chapterlist(key);

                      <?php
                      }
                      ?> 
                }
            }
        });
}

function chapterlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['question/chapterlist'])?>?subid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#questionset-chapterid').html('<option value="">Select Chapter</option>');
                    var chapter=JSON.parse(results);
                    $.each(chapter,function(key,value){
                        $('#questionset-chapterid').append('<option value="'+value.ChapterId+'">'+value.ChapterName+'</option>');
                    });
                    <?php
                      if ($classid != '') {
                      ?>
                        $("#questionset-chapterid").val(<?=$chapterid?>);

                      <?php
                      }
                      ?>
                }
            }
        });
}
</script>
