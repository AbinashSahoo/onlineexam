<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Result */

$this->title = 'Update Result: ' . $model->ResultId;
$this->params['breadcrumbs'][] = ['label' => 'Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ResultId, 'url' => ['view', 'id' => $model->ResultId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="result-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subject' => $subject,
        'studentid' => $studentid,
        'questionset' => $questionset,
    ]) ?>

</div>
