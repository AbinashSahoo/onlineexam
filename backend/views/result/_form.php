<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Result */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="result-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'StudentMark')->textInput() ?>

    <?= $form->field($model, 'SubjectId')->dropDownList($subject, ['prompt' => 'Choose Subject...'])?>

    <?= $form->field($model, 'StudentId')->dropDownList($studentid, ['prompt' => 'Choose Student...'])?>

    <?= $form->field($model, 'QuestionSetId')->dropDownList($questionset, ['prompt' => 'Choose Set...'])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
