<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tutorial */

$this->title = 'Create Tutorial';
$this->params['breadcrumbs'][] = ['label' => 'Tutorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tutorial-create">

  

    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes,
        'teacher' => $teacher,
    ]) ?>

</div>
