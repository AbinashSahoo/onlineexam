<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tutorial */

$this->title = 'Update Tutorial: ' . $model->TutorialId;
$this->params['breadcrumbs'][] = ['label' => 'Tutorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TutorialId, 'url' => ['view', 'id' => $model->TutorialId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tutorial-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes,
        'teacher' => $teacher,
    ]) ?>

</div>
