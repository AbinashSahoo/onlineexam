<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TutorialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tutorials';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Tutorial</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<div class="row">				
				
				<div class="col-12">
				  <div class="box">
					<div class="box-header with-border">
					  <h4 class="box-title">View Tutorial </h4>
					  <div class="box-controls pull-right">
						<div class="lookup lookup-circle lookup-right">
						  <input type="text" name="s">
						</div>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="table-responsive">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'tableOptions' => ['class' => 'table table-hover table-striped'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'TutorialId',
            [
                'attribute' => 'TeacherId',
                'value' => function ($data) {
                  if ($data->teacher) {
                    return $data->teacher->userdateils->Name;;
                  }
                },
            ],
            [
                'attribute' => 'ClassId',
                'value' => function ($data) {
                  if ($data->classes) {
                    return $data->classes->ClassName."(".$data->classes->SectionName.")";
                  }
                },
            ],
            [
                'attribute' => 'SubjectId',
                'value' => function ($data) {
                  if ($data->subject) {
                    return $data->subject->SubjectName;
                  }
                },
            ],
            [
                'attribute' => 'ChapterId',
                'value' => function ($data) {
                  if ($data->chapter) {
                    return $data->chapter->ChapterName;
                  }
                },
            ],
            // 'Video',
            'Description',
            //'IsDelete',
            'Ondate',
            //'UpdateDate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
					</div>
					<!-- /.box-body -->
				  </div>
				</div>
				
				
				
				
				
			  </div>			
		</section>
