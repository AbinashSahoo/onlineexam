<?php

namespace backend\controllers;

use Yii;
use common\models\Tutorial;
use common\models\TutorialSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Subject;
use common\models\Classes;
use common\models\Chapter;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * TutorialController implements the CRUD actions for Tutorial model.
 */
class TutorialController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tutorial models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TutorialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tutorial model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tutorial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tutorial();
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});
        $teacher = ArrayHelper::map(User::find()->where(['IsDelete' => 0, 'RoleId' => 2])->all(), 'UserId', function ($model) {
            return ($model->userdateils)?$model->userdateils->Name:$model->UserName; });

        if ($model->load(Yii::$app->request->post())) {
            $video=UploadedFile::getInstance($model, 'Video');
            $fileend=explode(".", $video->name);
            $ext = end($fileend);
            $basepath=Yii::getAlias('@storage');
            Yii::$app->params['uploadPath'] = $basepath . '/tutorial/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
            $model->Video = $ppp.".{$ext}";
            $video->saveAs($path);

            $SubjectDoc=UploadedFile::getInstance($model, 'SubjectDoc');
            $fileend=explode(".", $SubjectDoc->name);
            $ext = end($fileend);
            $basepath=Yii::getAlias('@storage');
            Yii::$app->params['uploadPath'] = $basepath . '/tutorial/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
            $model->SubjectDoc = $ppp.".{$ext}";
            $SubjectDoc->saveAs($path);
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Tutorial is uploaded successfully");
                return $this->redirect(['index']);
            } else {
                // var_dump($model->getErrors());die();
                Yii::$app->session->setFlash('error', "There is some error!");
            }
            
        }

        return $this->render('create', [
            'model' => $model,
            'classes' => $classes,
            'teacher' => $teacher,
        ]);
    }

    /**
     * Updates an existing Tutorial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});
        $teacher = ArrayHelper::map(User::find()->where(['IsDelete' => 0, 'RoleId' => 2])->all(), 'UserId', function ($model) {
            return $model->userdateils->Name; });

        if ($model->load(Yii::$app->request->post())) {
            $video=UploadedFile::getInstance($model, 'Video');
            if ($video) {
                $fileend=explode(".", $video->name);
                $ext = end($fileend);
                $basepath=Yii::getAlias('@storage');
                Yii::$app->params['uploadPath'] = $basepath . '/tutorial/';
                $ppp=Yii::$app->security->generateRandomString();
                $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
                $model->Video = $ppp.".{$ext}";
                $video->saveAs($path);
            }else{
                $model->Video = Yii::$app->request->post()['backupvideo'];
            }
            $SubjectDoc=UploadedFile::getInstance($model, 'SubjectDoc');
            if ($SubjectDoc) {
            $fileend=explode(".", $SubjectDoc->name);
            $ext = end($fileend);
            $basepath=Yii::getAlias('@storage');
            Yii::$app->params['uploadPath'] = $basepath . '/tutorial/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
            $model->SubjectDoc = $ppp.".{$ext}";
            $SubjectDoc->saveAs($path);
            }else{
                $model->SubjectDoc = Yii::$app->request->post()['backupsubdoc'];
            }
            if($model->save())
            {
                Yii::$app->session->setFlash('success','User Update Successfully');
                return $this->redirect(['index']);
            }
            else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }
            
        }

        return $this->render('update', [
            'model' => $model,
            'classes' => $classes,
            'teacher' => $teacher,
        ]);
    }

    /**
     * Deletes an existing Tutorial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->IsDelete = 1;
        if ($model->save()) {
            // var_dump($model->getErrors());die();   
            Yii::$app->session->setFlash('success', "Subject deleted successfully");
        } else {

            //var_dump($model->getErrors());die();
            Yii::$app->session->setFlash('error', "There is some error!");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tutorial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tutorial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tutorial::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
