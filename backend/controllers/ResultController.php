<?php

namespace backend\controllers;

use Yii;
use common\models\Result;
use common\models\ResultSearch;
use common\models\Subject;
use common\models\Questionset;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\Answer;
use common\models\Question;
use common\models\Examenable;

/**
 * ResultController implements the CRUD actions for Result model.
 */
class ResultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Result models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ResultSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Result model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Result model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Result();
        $subject = ArrayHelper::map(Subject::find()->where(['IsDelete' => 0])->all(), 'SubjectId','SubjectName');
        $studentid = ArrayHelper::map(User::find()->where(['IsDelete' => 0, 'RoleId' => 3])->all(), 'UserId','UserName');
        $questionset = ArrayHelper::map(Questionset::find()->where(['IsDelete' => 0])->all(), 'questionsetId','SetName');

        if ($model->load(Yii::$app->request->post())) {
               if($model->save())
               {
                Yii::$app->session->setFlash('success','User added Successfully');
                return $this->redirect(['index']);
            }
            else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }
        }

        return $this->render('create', [
            'model' => $model,
            'subject' => $subject,
            'studentid' => $studentid,
            'questionset' => $questionset,
        ]);
    }

    /**
     * Updates an existing Result model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $subject = ArrayHelper::map(Subject::find()->where(['IsDelete' => 0])->all(), 'SubjectId','SubjectName');
        $studentid = ArrayHelper::map(User::find()->where(['IsDelete' => 0, 'RoleId' => 3])->all(), 'UserId','UserName');
        $questionset = ArrayHelper::map(Questionset::find()->where(['IsDelete' => 0])->all(), 'questionsetId','SetName');

        if ($model->load(Yii::$app->request->post())) {
               if($model->save())
               {
                Yii::$app->session->setFlash('success','User added Successfully');
                return $this->redirect(['index']);
            }
            else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }
        }

        return $this->render('update', [
            'model' => $model,
            'subject' => $subject,
            'studentid' => $studentid,
            'questionset' => $questionset,
        ]);
    }

    /**
     * Deletes an existing Result model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Result model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Result the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Result::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // public function actionCalculateResult()
    // {
    //     $examstop = Examenable::find()
        
    //     // $qsndata = Question::find()->select(['SUM(question.QuestionMark) as fullmark','answer.*'])->joinWith(['answer'])->where(['answer.IsDelete'=>0])->groupBy(['answer.UserId','answer.ExamId'])->all();
    //     $connection = Yii::$app->getDb();
    //     $command = $connection->createCommand("
    //         SELECT SUM(question.QuestionMark) AS `fullmark`, `answer`.*, `question`.* FROM `question` LEFT JOIN `answer` ON `question`.`QuestionId` = `answer`.`QuestionId` AND `question`.`CorrectAns` = `answer`.`Answer` WHERE `answer`.`IsDelete`=0 GROUP BY `answer`.`UserId`, `answer`.`ExamId`");
    //     $result = $command->queryAll();
           
    // }
}
