<?php

namespace backend\controllers;

use Yii;
use common\models\Notice;
use common\models\NoticeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\Classes;

/**
 * NoticeController implements the CRUD actions for Notice model.
 */
class NoticeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NoticeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notice model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notice();
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName;});

        if ($model->load(Yii::$app->request->post())) {
                if($model->save())
            {
                Yii::$app->session->setFlash('success','Notice added Successfully');
                return $this->redirect(['index']);
            }else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }
        }

        return $this->render('create', [
            'model' => $model,
            'classes' => $classes
        ]);
    }

    /**
     * Updates an existing Notice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName;});

        if ($model->load(Yii::$app->request->post())) {
           if($model->save()) {
            Yii::$app->session->setFlash('success','Notice Updated Successfully');
            return $this->redirect(['index']);
            }
            else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }
        }

        return $this->render('update', [
            'model' => $model,
            'classes' => $classes
        ]);
    }

    /**
     * Deletes an existing Notice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->IsDelete = 1;
        if ($model->save()) {
            // var_dump($model->getErrors());die();   
            Yii::$app->session->setFlash('success', "Notice removed successfully");
        } else {

            //var_dump($model->getErrors());die();
            Yii::$app->session->setFlash('error', "There is some error!");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notice::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
