<?php

namespace backend\controllers;

use Yii;
use common\models\Questionset;
use common\models\QuestionsetSearch;
use common\models\Subject;
use common\models\Classes;
use common\models\Chapter;
use common\models\Question;
use common\models\Examenable;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QuestionsetController implements the CRUD actions for Questionset model.
 */
class QuestionsetController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                //     'delete' => ['POST'],
                // ],
            ],
        ];
    }

    /**
     * Lists all Questionset models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionsetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $classes = Classes::find()->where(['IsDelete' => 0])->all();
        $classid = $subjectid = $chapterid = $questionsets = '';

        if (isset(Yii::$app->request->post()['search'])) {
            $classid = Yii::$app->request->post('ClassId');
            $subjectid = Yii::$app->request->post('SubjectId');
            $chapterid = Yii::$app->request->post('ChapterId');

            $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$classid,$subjectid,$chapterid);              
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'classes' => $classes,
            'classid' => $classid,
            'subjectid' => $subjectid,
            'chapterid' => $chapterid,
        ]);
    }

    /**
     * Displays a single Questionset model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Questionset model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Questionset();
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});

        $classid = $subjectid = $chapterid = $questions = $setname = '';
        if (isset(Yii::$app->request->post()['question'])) {
            $classid = Yii::$app->request->post()['Questionset']['ClassId'];
            $subjectid = Yii::$app->request->post()['Questionset']['SubjetId'];
            $chapterid = Yii::$app->request->post()['Questionset']['ChapterId'];
            $setname = Yii::$app->request->post()['Questionset']['SetName'];

            $questions = Question::find()->where(['ClassId' => $classid ,'SubjectId' => $subjectid,'ChapterId' => $chapterid ,'IsDelete' => 0])->all();           
        }

        if (isset(Yii::$app->request->post()['save'])) {
           foreach (Yii::$app->request->post('QuestionId') as $key => $qids) {
                $model = new Questionset();
                $model->SetName = Yii::$app->request->post()['Questionset']['SetName'];
                $model->ClassId = Yii::$app->request->post()['Questionset']['ClassId'];
                $model->SubjetId = Yii::$app->request->post()['Questionset']['SubjetId'];
                $model->ChapterId = Yii::$app->request->post()['Questionset']['ChapterId']; 
                $model->QuestionId = intval($qids);
                $model->save();
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Questionset is successfull");
                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());die();
                Yii::$app->session->setFlash('error', "There is some error!");
            }
        }
        return $this->render('create', [
            'model' => $model,
            'classes' => $classes,
            'classid' => $classid,
            'subjectid' => $subjectid,
            'chapterid' => $chapterid,
            'setname' => $setname,
            'questions' => $questions,
        ]);
    }

    /**
     * Updates an existing Questionset model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});
        $subject = ArrayHelper::map(Subject::find()->where(['IsDelete' => 0])->all(), 'SubjectId','SubjectName');
        $chapter = ArrayHelper::map(Chapter::find()->where(['IsDelete' => 0])->all(), 'ChapterId','ChapterName');

            if ($model->load(Yii::$app->request->post())) {
               if($model->save())
               {
                Yii::$app->session->setFlash('success','User added Successfully');
                return $this->redirect(['index']);
            }
            else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }
        }
        return $this->render('create', [
            'model' => $model,
            'classes' => $classes,
        ]);
    }

    public function actionViewqsns($classid,$subjectid,$setname)
    {
        $model = Questionset::find()
        ->where(['ClassId'=>$classid,'SubjetId'=>$subjectid,'SetName'=>$setname,'IsDelete'=>0])
        ->all();
        
        return $this->render('view_qustions', [
            'model' => $model,
            'SetName'=>$setname,
        ]);
    }

    public function actionExamstart($classid,$setname,$duration,$subid)
    {
        
        $exam = new Examenable();
        $exam->ClassId = $classid;
        $exam->SubjectId = $subid;
        $exam->Set = $setname;
        $exam->Duration = $duration;
        $nowtime = date('Y-m-d H:i:s');
        $exam->StartTime = $nowtime;
        $exam->EndTime = date('Y-m-d H:i:s', strtotime($nowtime . ' + '.$duration.' hour'));
        if($exam->save()){
            return true;
        }
        
        return false;
    }

    // public function actionEndexam()
    // {

    //     $currentTime=date('Y-m-d-H:i:s');
    //     if($currentTime)
    //     $end = Examenable::updateAll(['Status' => 1], ['and', ['=' ,'IsDelete',0], ['<', 'StartTime', $currentTime],["DATE_ADD(`StartTime`, INTERVAL 3 HOUR) > '$currentTime'"] ]);
    //     if($end){
    //         $res = $end;
    //     }else{
    //         $res='';
    //     }
        
    //     return $this->render('endexam', [
    //         'res'=>$res,
    //     ]);
    // }

    /**
     * Deletes an existing Questionset model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->IsDelete = 1;
        if ($model->save()) {
            // var_dump($model->getErrors());die();
            echo json_encode(true);   
            // return $this->redirect(['viewqsns']);
        } else {

            //var_dump($model->getErrors());die();
            echo json_encode(false);
        }

        // return $this->redirect(['index']);
    }

    /**
     * Finds the Questionset model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questionset the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questionset::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
