<?php

namespace backend\controllers;

use Yii;
use common\models\Userdetails;
use common\models\UserdetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\Classes;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use common\models\Role;

/**
 * UserdetailsController implements the CRUD actions for Userdetails model.
 */
class UserdetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Userdetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserdetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Userdetails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Userdetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function Documentupload($document)
    {
        $model = new Userdetails();

        $image=UploadedFile::getInstance($model, $document);
        if($image){
           $fileend=explode(".", $image->name);
            $ext = end($fileend);
            $basepath=Yii::getAlias('@storage');
            Yii::$app->params['uploadPath'] = $basepath . '/document/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
            $documentname = $ppp.".{$ext}";
            $image->saveAs($path); 
        }else{
            $documentname = '';
        }
        

        return $documentname;
    }

    public function actionCreate()
    {
        $model = new Userdetails();
        $user = new User();
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});
        $role = ArrayHelper::map(Role::find()->where(['IsDelete' => 0])->all(), 'RoleId', 'RoleName');

        if ($model->load(Yii::$app->request->post())) {
            $model->UsertPhoto = $this->Documentupload('UsertPhoto');
            $model->JointPhoto = $this->Documentupload('JointPhoto');
            $model->UserAdhar = $this->Documentupload('UserAdhar');
            $model->Fadhar = $this->Documentupload('Fadhar');
            $model->Madhar = $this->Documentupload('Madhar');
            $model->AdmissionForm = $this->Documentupload('AdmissionForm');
            $model->BirthCertificate = $this->Documentupload('BirthCertificate');
            $model->Qualifiaction = $this->Documentupload('Qualifiaction');
            $model->HighSchoolMarksheet = $this->Documentupload('HighSchoolMarksheet');
            $model->IntermediateMarksheet = $this->Documentupload('IntermediateMarksheet');
            $model->Graduation = $this->Documentupload('Graduation');
            $model->DOB = date("Y-m-d", strtotime(Yii::$app->request->post()['Userdetails']['DOB']));
            $model->DOA = date("Y-m-d", strtotime(Yii::$app->request->post()['Userdetails']['DOA']));
            $model->DOJ = date("Y-m-d", strtotime(Yii::$app->request->post()['Userdetails']['DOJ']));
            if($model->save())
            {
                $hash = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post()['User']['Password']);
                $user->UserName = $model->ResgistrationNumber;
                $user->Password = $hash;
                $user->RoleId = Yii::$app->request->post()['User']['RoleId'];
                $user->UserDetailsId = $model->UserDetailsId;
                if($user->save())
                {
                    Yii::$app->session->setFlash('success','User added Successfully');
                    return $this->redirect(['index']);
                }
                else{
                    Yii::$app->session->setFlash('error','Something went wrong!');
                }
            }else{
                Yii::$app->session->setFlash('error','Oops poor network!');
            } 
        }

        return $this->render('create', [
            'model' => $model,
            'classes' => $classes,
            'role' => $role,
            'user' => $user,
        ]);
    }

    /**
     * Updates an existing Userdetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $chkrole = $model->user->role->RoleId ;
        $user = User::find()->where(['UserDetailsId' => $id ,'IsDelete' => 0])->one();
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});
        $role = ArrayHelper::map(Role::find()->where(['IsDelete' => 0])->all(), 'RoleId', 'RoleName');

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->post()['Userdetails']['UsertPhoto'] != ''){
                $model->UsertPhoto = $this->Documentupload('UsertPhoto');
            }else{
                $model->UsertPhoto = Yii::$app->request->post()['oldUphoto'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['JointPhoto'] != ''){
                $model->JointPhoto = $this->Documentupload('JointPhoto');
            }else{
                $model->JointPhoto = Yii::$app->request->post()['oldJphoto'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['UserAdhar'] != ''){
                $model->UserAdhar = $this->Documentupload('UserAdhar');
            }else{
                $model->UserAdhar = Yii::$app->request->post()['oldUadhar'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['Fadhar'] != ''){
                $model->Fadhar = $this->Documentupload('Fadhar');
            }else{
                $model->Fadhar = Yii::$app->request->post()['oldFadhar'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['Madhar'] != ''){
                $model->Madhar = $this->Documentupload('Madhar');
            }else{
                $model->Madhar = Yii::$app->request->post()['oldMadhar'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['AdmissionForm'] != ''){
                $model->AdmissionForm = $this->Documentupload('AdmissionForm');
            }else{
                $model->AdmissionForm = Yii::$app->request->post()['oldAform'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['BirthCertificate'] != ''){
                $model->BirthCertificate = $this->Documentupload('BirthCertificate');
            }else{
                $model->BirthCertificate = Yii::$app->request->post()['oldBirth'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['Qualifiaction'] != ''){
                $model->Qualifiaction = $this->Documentupload('Qualifiaction');
            }else{
                $model->Qualifiaction = Yii::$app->request->post()['oldQual'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['HighSchoolMarksheet'] != ''){
                $model->HighSchoolMarksheet = $this->Documentupload('HighSchoolMarksheet');
            }else{
                $model->HighSchoolMarksheet = Yii::$app->request->post()['oldHsM'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['IntermediateMarksheet'] != ''){
                $model->IntermediateMarksheet = $this->Documentupload('IntermediateMarksheet');
            }else{
                $model->IntermediateMarksheet = Yii::$app->request->post()['oldIntM'] ;
            }
            if(Yii::$app->request->post()['Userdetails']['Graduation'] != ''){
                $model->Graduation = $this->Documentupload('Graduation');
            }else{
                $model->Graduation = Yii::$app->request->post()['oldGrad'] ;
            }

            $model->DOB = date("Y-m-d", strtotime(Yii::$app->request->post()['Userdetails']['DOB']));
            $model->DOA = date("Y-m-d", strtotime(Yii::$app->request->post()['Userdetails']['DOA']));
            $model->DOJ = date("Y-m-d", strtotime(Yii::$app->request->post()['Userdetails']['DOJ']));
            if ($model->save()) {
                $user->UserName = $model->ResgistrationNumber;
                $hash = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post()['User']['Password']);
                $user->UserName = $model->ResgistrationNumber;
                $user->Password = $hash;
                $user->RoleId = Yii::$app->request->post()['User']['RoleId'];
                $user->UserDetailsId = $model->UserDetailsId;
                if($user->save())
                {
                    Yii::$app->session->setFlash('success','User Update Successfully');
                    return $this->redirect(['index']);
                }
                else{
                    Yii::$app->session->setFlash('error','Something went wrong!');
                }
            }else{
                    Yii::$app->session->setFlash('error','Oops poor network!');
                }
            }

        return $this->render('update', [
            'model' => $model,
            'classes' => $classes,
            'role' => $role,
            'user' => $user,
            'chkrole' => $chkrole,
        ]);
    }

    /**
     * Deletes an existing Userdetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $user = User::find()->where(['UserDetailsId'=>$id,'IsDelete'=>0])->one();
        $model->IsDelete = 1;
        if ($model->save()) {
            $user->IsDelete=1;
            if($user->save()){
                // var_dump($model->getErrors());die();   
            Yii::$app->session->setFlash('success', "User deleted successfully");
            }else {

            //var_dump($model->getErrors());die();
            Yii::$app->session->setFlash('error', "There is some error!");
        }
            
        } else {

            //var_dump($model->getErrors());die();
            Yii::$app->session->setFlash('error', "There is some error!");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Userdetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Userdetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Userdetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
