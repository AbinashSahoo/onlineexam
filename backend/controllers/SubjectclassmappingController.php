<?php

namespace backend\controllers;

use Yii;
use common\models\Subjectclassmapping;
use common\models\SubjectclassmappingSearch;
use common\models\Subject;
use common\models\Classes;
use common\models\Chapter;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * SubjectclassmappingController implements the CRUD actions for Subjectclassmapping model.
 */
class SubjectclassmappingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Subjectclassmapping models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubjectclassmappingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Subjectclassmapping model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Subjectclassmapping model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subjectclassmapping();
        $subject = ArrayHelper::map(Subject::find()->where(['IsDelete' => 0])->all(), 'SubjectId','SubjectName');
        $teacherid = ArrayHelper::map(User::find()->where(['IsDelete' => 0, 'RoleId' => 2])->all(), 'UserId', function ($model) {
            return $model->userdateils->Name; });
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});
        $chapter = ArrayHelper::map(Chapter::find()->where(['IsDelete' => 0])->all(), 'ChapterId','ChapterName');

        if ($model->load(Yii::$app->request->post())) {
               if($model->save())
               {
                Yii::$app->session->setFlash('success','Mapping added Successfully');
                return $this->redirect(['index']);
            }
            else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }
        }

        return $this->render('create', [
            'model' => $model,
            'teacherid' => $teacherid,
            'classes' => $classes,
        ]);
    }

    public function actionSubjectlist($classid)
    {
        $subject = Subject::find()->where(['Classid' => $classid,'IsDelete' => 0])->asArray()->all();

        echo json_encode($subject);
    }

    public function actionChapterlist($subid)
    {
        $chapter = Chapter::find()->where(['SubjectId' => $subid,'IsDelete' => 0])->asArray()->all();

        echo json_encode($chapter);
    }

    /**
     * Updates an existing Subjectclassmapping model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $subject = ArrayHelper::map(Subject::find()->where(['IsDelete' => 0])->all(), 'SubjectId','SubjectName');
        $teacherid = ArrayHelper::map(User::find()->where(['IsDelete' => 0, 'RoleId' => 2])->all(), 'UserId', function ($model) {
            return $model->userdateils->Name; });
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});
        $chapter = ArrayHelper::map(Chapter::find()->where(['IsDelete' => 0])->all(), 'ChapterId','ChapterName');

        if ($model->load(Yii::$app->request->post())) {
               if($model->save())
               {
                Yii::$app->session->setFlash('success','Mapping added Successfully');
                return $this->redirect(['index']);
            }
            else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }
        }

        return $this->render('update', [
            'model' => $model,
            'subject' => $subject,
            'teacherid' => $teacherid,
            'classes' => $classes,
            'chapter' => $chapter,
        ]);
    }

    /**
     * Deletes an existing Subjectclassmapping model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->IsDelete = 1;
        if ($model->save()) {
            // var_dump($model->getErrors());die();   
            Yii::$app->session->setFlash('success', "Mapping deleted successfully");
        } else {

            //var_dump($model->getErrors());die();
            Yii::$app->session->setFlash('error', "There is some error!");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Subjectclassmapping model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subjectclassmapping the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subjectclassmapping::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
