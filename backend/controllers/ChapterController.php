<?php

namespace backend\controllers;

use Yii;
use common\models\Chapter;
use common\models\ChapterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Subject;
use common\models\Classes;
use yii\helpers\ArrayHelper;

/**
 * ChapterController implements the CRUD actions for Chapter model.
 */
class ChapterController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Chapter models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChapterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Chapter model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Chapter model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Chapter();
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success','Chapter added Successfully');
                return $this->redirect(['index']);
            }else{
                Yii::$app->session->setFlash('error','Something went wrong!');
            }  
        }

        return $this->render('create', [
            'model' => $model,
            'classes' => $classes,
        ]);
    }

    public function actionSubjectlist($classid)
    {
        $subject = Subject::find()->where(['Classid' => $classid,'IsDelete' => 0])->asArray()->all();

        echo json_encode($subject);
    }

    /**
     * Updates an existing Chapter model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $classes = ArrayHelper::map(Classes::find()->where(['IsDelete' => 0])->all(), 'ClassId', function ($model) {
            return $model->ClassName . ' - ' . $model->SectionName;});
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success','Chapter Updtaed Successfully');
               return $this->redirect(['index']);
           }else{
            Yii::$app->session->setFlash('error','Something went wrong!');
        }   
    }

    return $this->render('update', [
        'model' => $model,
        'classes' => $classes,
    ]);
}

    /**
     * Deletes an existing Chapter model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->IsDelete = 1;
        if ($model->save()) {
            // var_dump($model->getErrors());die();   
            Yii::$app->session->setFlash('success', "Chapter deleted successfully");
        } else {

            //var_dump($model->getErrors());die();
            Yii::$app->session->setFlash('error', "There is some error!");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Chapter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chapter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chapter::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
