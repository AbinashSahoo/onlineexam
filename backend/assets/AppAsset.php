<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/style.css',
        'css/skin_color.css',
    ];
    public $js = [
    //'app.js',
	'js/apexcharts.js',
	'js/jquery.flot.js',
	'js/jquery.flot.pie.js',
	'js/template.js',
	'js/demo.js',
    'js/tinymce.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
		'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
