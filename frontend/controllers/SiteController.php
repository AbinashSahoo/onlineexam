<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Tutorial;
use common\models\Userdetails;
use common\models\Subject;
use common\models\Classes;
use common\models\Chapter;
use common\models\Examenable;
use common\models\Questionset;
use yii\data\SqlDataProvider;
use common\models\Notice;
use common\models\Answer;
use yii\data\Pagination;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['profile']);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */

    public function actionLogin()
    {
		$this->layout = "login";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = yii::$app->user->identity->RoleId;
            if($user != '3'){
                Yii::$app->user->logout();
                Yii::$app->session->setFlash('error', 'You are not an authorized user');
                return $this->goHome();
            }
            return $this->goBack();
        } else {
            $model->Password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays Exam page.
     *
     * @return mixed
     */

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function actionTutorial()
    {
        $uid = Yii::$app->user->identity->UserDetailsId;
        $user = Userdetails::find()->where(['UserDetailsId' => $uid,'IsDelete'=>0])->one();
        $classid = $user->Classid;
        if (isset(Yii::$app->request->post()['search'])) {
            $subjectid = Yii::$app->request->post()['SubjectId'];
            $chapterid = Yii::$app->request->post()['ChapterId'];
            $subname = Subject::find()->where(['SubjectId' => $subjectid,'IsDelete' => 0])->one();
            $chapname = Chapter::find()->where(['ChapterId' => $chapterid,'IsDelete' => 0])->one();
            $tutorial = Tutorial::find()
            ->where(['Classid'=>$classid,'SubjectId'=>$subjectid,'ChapterId'=>$chapterid,'IsDelete'=>0])
            ->all();
        }else{
            $tutorial =$subname = $chapname= '';
        }
        // var_dump($tutorial);die();
        return $this->render('studenttutorial', [
            'classid' => $classid,
            'tutorial' => $tutorial,
            'subname' => $subname,
            'chapname' => $chapname,
        ]);
    }

    public function actionProfile()
    {
        $userid = Yii::$app->user->identity->UserDetailsId;
        $profile = Userdetails::find()->where(['UserDetailsId' => $userid,'IsDelete'=>0])->one();
        
        
        return $this->render('profile', [
            'profile' => $profile
        ]);
    }

    public function actionSubjectlist($classid)
    {
        $subject = Subject::find()->where(['Classid' => $classid,'IsDelete' => 0])->asArray()->all();

        echo json_encode($subject);
    }

    public function actionChapterlist($subid)
    {
        $chapter = Chapter::find()->where(['SubjectId' => $subid,'IsDelete' => 0])->asArray()->all();

        echo json_encode($chapter);
    }

    public function actionExam()
    {
        $uid = Yii::$app->user->identity->UserDetailsId;
        $classid = Userdetails::find()->where(['UserDetailsId' => $uid,'IsDelete'=>0])->one();
        $questions = Examenable::find()->where(['ClassId' => $classid->Classid,'IsDelete'=>0])->all();
        return $this->render('exam', [
            'questions' => $questions
        ]);
    }

    public function actionExampanel($classid,$subjectid)
    {
        $timing = Examenable::find()->where(['ClassId' => $classid,'SubjectId' => $subjectid,'IsDelete'=>0])->one();
        $count = Questionset::find()
        ->where(['ClassId'=>$classid,'SubjetId'=>$subjectid,'IsDelete'=>0])
        ->count();
        $sql = Questionset::find()
        ->where(['ClassId'=>$classid,'SubjetId'=>$subjectid,'IsDelete'=>0]);
        
        $countQuery = clone $sql;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize'=>1]);
        $models = $sql->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $currenttime = strtotime(date('Y-m-d H:i:s'));
        $endtime = strtotime($timing->EndTime);
        $duration = ($currenttime - $endtime)*1000;
        return $this->render('exampanel', [
            'models' => $models,
            'pages' => $pages,
            'duration' => $duration,
            'endtime'=> $endtime*1000,
            'endtimeforexam'=> $timing->EndTime,
            'timing'=>$timing,
        ]);

    }
    public function actionNotice()
    {
        $uid = Yii::$app->user->identity->UserDetailsId;
        $classid = Userdetails::find()->where(['UserDetailsId' => $uid,'IsDelete'=>0])->one();
        $notices = Notice::find()
        ->where(['ClassId'=>$classid->Classid,'IsDelete'=>0])
        ->all();
        return $this->render('studentnotice', [
            'notices' => $notices,
            
        ]);
    }

    public function actionAnswerSubmit($ans,$qid,$endexam,$examid)
    {
        $uid = Yii::$app->user->identity->UserDetailsId;
        $model = new Answer();
        $checkAns = Answer::find()->where(['QuestionId' => $qid,'UserId' => $uid,'IsDelete'=>0])->one();
        $currenttime = date('Y-m-d H:i:s');
        if($checkAns){
            $checkAns->Answer = strval($ans);
            if ($endexam >= $currenttime) {
                if ($checkAns->save()) {
                    return true;
                }else{
                    return false;
                }
                
            }else{
                return false;
            }
        }else{
            $model->QuestionId = $qid;
            $model->UserId = $uid;
            $model->Answer = strval($ans);
            $model->ExamId = $examid;
            if ($endexam >= $currenttime) {
                if ($model->save()) {
                    return true;
                }else{
                    return false;
                }
                
            }else{
                return false;
            }
        }
    }
}
