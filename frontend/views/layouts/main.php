<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
AppAsset::register($this);
$frontendUrl=Yii::getAlias('@frontendUrl');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title>Online Exam</title>
    <?php $this->head() ?>
</head>
<body class="light-skin sidebar-mini theme-deepocean">
    <?php $this->beginBody() ?>
<div class="wrapper">
	
  <div class="art-bg" style="background: #1d5f98;">
	
  </div>

	<header class="main-header">
    <!-- Logo -->
    <a href="index.html" class="logo">
      <!-- mini logo -->
	  <div class="logo-mini">
		  <span class="light-logo"><img src="<?= $frontendUrl;?>/images/logo-light.png" alt="logo" style="width:45px; height:45px;"></span>
		 
	  </div>
      <!-- logo-->
      <div class="logo-lg">
		  <span class="light-logo" style="color:#fff; font-weight:bold;">K-Online Test</span>
	  </div>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
		
	  <div class="app-menu">
		<ul class="header-megamenu nav">
			<li class="btn-group nav-item">
				<a href="#" class="nav-link rounded" data-toggle="push-menu" role="button">
					<i class="nav-link-icon mdi mdi-menu text-white"></i>
			    </a>
			</li>
				
			
		</ul> 
	  </div>
		
      <!-- <div class="navbar-custom-menu r-side">
        <ul class="nav navbar-nav"> -->
		  
		  <!-- User Account-->
          <!-- <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="User">
              <img src="<?= $frontendUrl;?>/images/7.jpg" class="user-image rounded-circle" alt="User Image">
            </a>
            <ul class="dropdown-menu animated flipInX"> -->
              <!-- User image -->
              <!-- <li class="user-header bg-img" style="background-image: url(images/user-info.jpg)" data-overlay="3">
				  <div class="flexbox align-self-center">					  
				  	<img src="<?= $frontendUrl;?>/images/7.jpg" class="float-left rounded-circle" alt="User Image">					  
					<h4 class="user-name align-self-center">
					  <span>Samuel Brus</span>
					  <small>samuel@gmail.com</small>
					</h4>
				  </div>
              </li> -->
              <!-- Menu Body -->
              <!-- <li class="user-body">
				    <a class="dropdown-item" href="javascript:void(0)"><i class="ion ion-person"></i> My Profile</a>
					<a class="dropdown-item" href="javascript:void(0)"><i class="ion ion-bag"></i> My Balance</a>
					<a class="dropdown-item" href="javascript:void(0)"><i class="ion ion-email-unread"></i> Inbox</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="javascript:void(0)"><i class="ion ion-settings"></i> Account Setting</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="javascript:void(0)"><i class="ion-log-out"></i> Logout</a>
					<div class="dropdown-divider"></div>
					<div class="p-10"><a href="javascript:void(0)" class="btn btn-sm btn-rounded btn-success">View Profile</a></div>
              </li>
            </ul>
          </li>	
			
        </ul>
      </div> -->
    </nav>
  </header>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full clearfix position-relative">			
    
	<aside class="main-sidebar">
			<!-- sidebar-->
			<section class="sidebar">
			  <!-- sidebar menu-->
			  <ul class="sidebar-menu" data-widget="tree">

				

				<li class="active">
				  <a href="<?= Url::toRoute(['site/profile']); ?>">
					<i class="ti-user"></i>
					<span>Profile</span>
				  </a>
				</li>
				  
				
				<li class="treeview">
				  <a href="#">
					<i class="ti-comment-alt"></i>
					<span>Notice</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu">
					
					<li><a href="<?= Url::toRoute(['site/notice']); ?>"><i class="ti-more"></i>View notice</a></li>
				  </ul>
				</li>
				
				<li class="treeview">
				  <a href="#">
					<i class="ti-write"></i>
					<span>Tutorial</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu">
					
					<li><a href="<?= Url::toRoute(['site/tutorial']); ?>"><i class="ti-more"></i>View Tutorial</a></li>
				  </ul>
				</li>
				<li class="header nav-small-cap">Exam</li> 

				<li class="treeview">
				  <a href="#">
					<i class="ti-clipboard"></i>
					<span>Exam</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu">			
					<li><a href="<?= Url::toRoute(['site/exam']); ?>"><i class="ti-more"></i>Exam</a></li>
					
				  </ul>
				</li>
				

				<li>
				  <a href="<?= Url::toRoute(['site/logout']); ?>" data-method="post">
					<i class="ti-power-off"></i>
					<span>Logout</span>
				  </a>
				</li> 

			  </ul>
			</section>
		</aside>
	
      
      
            
            <?= Alert::widget() ?>
            <?= $content ?>
       </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->  
     <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
		  <li class="nav-item">
			<a class="nav-link" href="javascript:void(0)"></a>
		  </li>
		  
		</ul>
    </div>
	  &copy; 2020 <a href="#">Online Exam</a>. All Rights Reserved.
  </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>




  
   
       

  