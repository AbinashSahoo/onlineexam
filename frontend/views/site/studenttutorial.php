<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Tutorial';
?>
<style>
.mainvideo{border:1px solid #dedede; padding:10px; min-height:250px; overflow:auto;}
.videobox{border:1px solid #dedede; }
</style>
<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Tutorial</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d')?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>
			
<?php
    if (!$tutorial) {?>
	<div class="row">				
				
        <div class="col-lg-12 col-12" id="for-search">
		 <div class="box">
						<div class="box-header with-border">
						  <h4 class="box-title">Tutorial</h4>
						</div>
    <?php $form = ActiveForm::begin(); ?>
		<div class="box-body">
        <select id="subject-id" name="SubjectId" class="form-control" required onchange="chapterlist(this.value);">
            <option value="">Choose Subject</option>
        </select><br/>

        <select id="chapter-id" name="ChapterId" class="form-control" required>
            <option value="">Choose Chapter</option>
        </select>
		</div>
        <div class="box-footer">
            <?= Html::submitButton('<i class="ti-search"></i> Search', ['class' => 'btn btn-primary','name' => 'search']) ?>
        </div>
    <?php ActiveForm::end(); ?>
	</div>
		</div>
		</div>
   <?php }
?>

<?php 
if ($tutorial) {?>
<div class="row">
    <div class="col-lg-12 col-12" id="tut-res">
	 <div class="box">
						<div class="box-header with-border">
						  <h4 class="box-title"><?=$subname->SubjectName?><br/> <span style="color:#1d5f98; font-size:15px;"><?=$chapname->ChapterName?></span></h4>
						</div>
       
		<div class="box-body col-lg-12">
		<div class="row">
			<div  class="col-lg-6 " >
			<div class="mainvideo">
				<video id="mainvid" style="width:100%; height:200px;" controls controlsList="nodownload">
				  <source src="<?=Yii::getAlias('@storageUrl')."/tutorial/".$tutorial[0]->Video;?>" type="video/mp4">
				</video>
			 </div> 
			</div>
		
			<div class="col-lg-6" >
				<div class="mainvideo">
				 <p id="desc"><?=$tutorial[0]->Description?></p>
                 <a id="mainsubdoc" href="<?=Yii::getAlias('@storageUrl')."/tutorial/".$tutorial[0]->SubjectDoc?>" download><button class="btn btn-info btn-sm mb-5"><i class="fa fa-download"></i> Download PDF</button></a>
				</div>
			</div>
		</div>
		
		<div class="col-lg-12" style="margin-top:15px;">
		<div class="row">
			<h4 class="box-title" style="width:100%;">Upnext</h4>
                <?php
                foreach ($tutorial as $key => $value) {?>
                    <div class="col-lg-3"style="  " >
						<div class="videobox text-center">
						<h5 style="background: #1d5f98;color:#fff; text-align:center; padding:5px;"><?=$value->chapter->ChapterName?></h5>
                        <video width="100%" height="150" id="v<?=$key?>">
                          <source src="<?=Yii::getAlias('@storageUrl')."/tutorial/".$value->Video;?>" type="video/mp4">
                        </video>
                        <a id="subdoc" href="<?=Yii::getAlias('@storageUrl')."/tutorial/".$value->SubjectDoc?>" download><button class="btn btn-info btn-sm mb-5"><i class="fa fa-download"></i> Download PDF</button></a>
                        <input type="hidden" id="vdescription" value="<?=$value->Description?>">
                    </div>
                </div>
               <?php }
                ?>
				
		</div>
		</div>
	</div>
		
		
		</div>
		
		</div>
    </div>
	
<?php }?>
</section>
<script type="text/javascript">
<?php
if ($classid != '') {?>
    setTimeout(function() {
  subjectlist(<?=$classid?>);
}, 1000);
<?php }
?>
function subjectlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['site/subjectlist'])?>?classid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#subject-id').html('<option value="">Select Subject</option>');
                    var subject=JSON.parse(results);
                    $.each(subject,function(key,value){
                        $('#subject-id').append('<option value="'+value.SubjectId+'">'+value.SubjectName+'</option>');
                    });
                    
                }
            }
        });
}

function chapterlist(value)
{
    $.ajax({url:"<?=Url::toRoute(['site/chapterlist'])?>?subid="+value,
            success:function(results)
            {
                if(results)
                {
                    $('#chapter-id').html('<option value="">Select Chapter</option>');
                    var chapter=JSON.parse(results);
                    $.each(chapter,function(key,value){
                        $('#chapter-id').append('<option value="'+value.ChapterId+'">'+value.ChapterName+'</option>');
                    });
                }
            }
        });
}
</script>
<?php
   $this->registerJs(
    "$('video').on('click',function(){
    var srcval=$(this).find('source').attr('src');
    var video = $('#mainvid').attr('src',srcval);
    video[0].play();
    $('#desc').html($(this).parent().find('#vdescription').val());
    var docval = $(this).parent().find('#subdoc').attr('href');
    $('#mainsubdoc').attr('href',docval);
    });"
    );
?>