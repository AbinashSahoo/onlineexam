<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Exam';
?>

<section class="content">
		<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Exam</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>
		<div class="row">				
				
				<div class="col-12">
				  <div class="box">
					<div class="box-header with-border">
					  <h4 class="box-title">View Exam </h4>
					  <div class="box-controls pull-right">
						<div class="lookup lookup-circle lookup-right">
						  <input type="text" name="s">
						</div>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="table-responsive">
							<table class="table table-hover table-striped add-edit-table">
								<thead>
									<td>Subject</td>
									<td>Number of Questions</td>
									<td>Action</td>
								</thead>
								<tbody>
									<?php
									foreach ($questions as $key => $value) {?>
										<tr>
											<td><?=$value->subject->SubjectName?></td>
											<td><?=$value->getQsncnt($value->ClassId,$value->SubjectId);?></td>
											<!-- <td></td> -->
											<td><?php $time = date('Y-m-d H:i:s');
											if($time >= $value->StartTime && $value->Status == 0 ){ 
											echo '<a href="'.Url::toRoute(['exampanel','classid'=>$value->ClassId,'subjectid'=>$value->SubjectId]).'"><button>Start</button></a>';
											} elseif($value->Status == 1 ){ 
											echo '<button>Expired</button>';
											 }else{ 
											echo 'Not Started';
											}?></td>
										</tr>
								   <?php ;}
									?>
								</tbody>
							</table>
						</div>
					</div>
			
				</div>
			</div>
		</div>
	</div>
	
</section>

