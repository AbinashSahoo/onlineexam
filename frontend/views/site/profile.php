<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Profile';
?>
<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Profile</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary">Jul 22</span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>
			
			
			<div class="row">
			  <div class="col-12">
			  	<div class="box box-inverse bg-img" style="background-image: url(../../images/gallery/full/1.jpg); background:#44c0f9;" data-overlay="2">
				 

				  <div class="box-body text-center pb-50">
					<a href="#">
					 <img class="avatar avatar-xxl avatar-bordered" src="<?=Yii::getAlias('@storageUrl')."/document/".$profile->UsertPhoto;?>" >
					  
					</a>
					<h4 class="mt-2 mb-0"><a class="hover-primary text-white" href="#"><?=$profile->Name?></a></h4>
					<span><?=$profile->BloodGroup?></span>
				  </div>

				  <ul class="box-body flexbox flex-justified text-center" data-overlay="4">
					<li>
					  <span class="opacity-60">Admission Number</span><br>
					  <span class="font-size-20" style="color: #f2cf11;"><?=$profile->AdmissionNumber?></span>
					</li>
					<li>
					  <span class="opacity-60">Registration Number</span><br>
					  <span class="font-size-20" style="color: #f2cf11;"><?=$profile->ResgistrationNumber?></span>
					</li>
					<li>
					  <span class="opacity-60">Joining Date</span><br>
					  <span class="font-size-20" style="color: #f2cf11;"><?=$profile->DOB?></span>
					</li>
					<li>
					  <span class="opacity-60">Admission Date</span><br>
					  <span class="font-size-20" style="color: #f2cf11;"><?=$profile->DOA?></span>
					</li>
				  </ul>
				</div>
			  </div>
			
			<!-- /.col -->		

			  <div class="col-12 col-lg-5 col-xl-6">
				<div class="box">
					<div class="box-body box-profile">            
					  <div class="row">
						<div class="col-12">
							<div>
								<p>DOB :<span class="text-gray pl-10"><?=$profile->DOB?></span> </p>
								<p>Mobile No :<span class="text-gray pl-10"><?=$profile->Mobile?></span></p>
								<p>Whatsapp :<span class="text-gray pl-10"><?=$profile->Whatsapp?></span></p>
								<p>Address :<span class="text-gray pl-10"><?=$profile->PresentAddress?></span></p>
								<p>Permanent Address :<span class="text-gray pl-10"><?=$profile->PermanentAddress?></span></p>
								<p>Present Address :<span class="text-gray pl-10"><?=$profile->PresentAddress?></span></p>
							</div>
						</div>
						
						
					  </div>
					</div>
					<!-- /.box-body -->
				  </div>
				  
			  </div>
			   <div class="col-12 col-lg-5 col-xl-6">
				<div class="box">
					<div class="box-body box-profile">            
					  <div class="row">
						<div class="col-12">
							<div>
								<p>Gender :<span class="text-gray pl-10"><?=$profile->Gender?></span> </p>
								<p>Father Name :<span class="text-gray pl-10"><?=$profile->Fname?></span> </p>
								<p>Mother Name :<span class="text-gray pl-10"><?=$profile->Mname?></span></p>
								<p>Nationality :<span class="text-gray pl-10"><?=$profile->Nationality?></span> </p>
								<p>Religion :<span class="text-gray pl-10"><?=$profile->Religion?></span></p>
								<p>Caste :<span class="text-gray pl-10"><?=$profile->Caste?></span></p>
								
								
							</div>
						</div>
						
						
					  </div>
					</div>
					<!-- /.box-body -->
				  </div>
				  
			  </div>

		  </div>
			
			
			
		<div class="row">
			<div class="site-index">
				
				
				

				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-2">
							<h4><u><i>JointPhoto</i></u></h4>
							<img src="<?=Yii::getAlias('@storageUrl')."/document/".$profile->JointPhoto;?>" width="100">
						</div>
						<div class="col-lg-2">
							<h4><u><i>UserAdhar</i></u></h4>
							<img src="<?=Yii::getAlias('@storageUrl')."/document/".$profile->UserAdhar;?>" width="100">
						</div>
						<div class="col-lg-2">
							<h4><u><i>Father's Aadhar</i></u></h4>
							<img src="<?=Yii::getAlias('@storageUrl')."/document/".$profile->Fadhar;?>" width="100">
						</div>
						<div class="col-lg-2">
							<h4><u><i>Mother's Aadhar</i></u></h4>
							<img src="<?=Yii::getAlias('@storageUrl')."/document/".$profile->Madhar;?>" width="100">
						</div>
						<div class="col-lg-2">
							<h4><u><i>Admission Form</i></u></h4>
							<img src="<?=Yii::getAlias('@storageUrl')."/document/".$profile->AdmissionForm;?>" width="100">
						</div>
						<div class="col-lg-2">
							<h4><u><i>Birth Certificate</i></u></h4>
							<img src="<?=Yii::getAlias('@storageUrl')."/document/".$profile->BirthCertificate;?>" width="100">
						</div>
					</div>
				</div>
			    
			    
			</div>
		</div>
	
</section>


