<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */

$this->title = 'Exam';
?>
<style>
	.time{width:35px; border-radius:5px;min-width:35px; float:left; text-align:center;font-weight: bold;color: #e04949; padding:5px;box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.5); background:#fff;}
	.qno{ width:50px; float:left; margin-right:10px;font-weight:600; border-radius:25px; padding:3px; text-align:center;background: #194175;color: #fff;}
</style>

<section class="content">
	<!-- Content Header (Page header) -->	  
	<div class="content-header">
		<div class="d-flex align-items-center justify-content-between">
			<div class="d-md-block d-none timers">
				<h3 class="page-title br-0">Remaing Times</h3>

				<ul style="list-style: none;padding: 0">
					<li style="list-style: none; float:left; display: inline-block; color:#fff; font-size: 25px;margin-right: 5px;"><i class="fa fa-clock-o"></i></li>
					<li style="list-style: none; float:left; display: inline-block; color:#fff;"><span id="hours" class="time"></span></li>
					<li style="list-style: none; float:left;display: inline-block;color:#fff;"><span style="float:left;margin:5px; margin-top:0px; font-size:20px;">:</span><span id="minutes" class="time"></span></li>
					<li style="list-style: none; float:left;display: inline-block;color:#fff;"><span style="float:left;margin:5px;margin-top:0px; font-size:20px;">:</span><span id="seconds" class="time"></span></li>
				</ul>
			</div>
			<div class="w-p60">

			</div>
			<div class="right-title w-170">
				<span class="subheader_daterange font-weight-600">
					<span class="subheader_daterange-label">
						<span class="subheader_daterange-title">Today:</span>
						<span class="subheader_daterange-date text-primary"><?=date('M d');?></span>
					</span>
					<a href="#" class="btn btn-rounded btn-sm btn-primary">
						<i class="fa fa-calendar"></i>
					</a>
				</span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">

			<div class="box">


				<div class="box-header with-border">
					<h4 class="box-title">Questions</h4>
					<div class="box-controls pull-right">
						<div class="lookup lookup-circle lookup-right">
							<input type="text" name="s">
						</div>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-9" style="margin-top:10px;" id="demo">
							<?php
							$a =1;
							foreach ($models as $key => $value) {?>

								<p> <span class="qno"> Q.<?=$pages->offset+1?> </span> <?=$value->question->Questions?></p>
								<div class="col-12" style="padding-left:50px;">
									<input type="radio" id="lida<?=$a?>" name="ans<?=$a?>" onchange="submitanswer(this.value,<?=$a?>,<?=$timing->ExamId?>);" value="A" data-id="<?=$a?>">
									<label for="lida<?=$a?>"><?=$value->question->Ans_a?></label><br/>
									<input type="radio" id="lidb<?=$a?>" name="ans<?=$a?>" onchange="submitanswer(this.value,<?=$a?>,<?=$timing->ExamId?>);" value="B" data-id="<?=$a?>">
									<label for="lidb<?=$a?>"><?=$value->question->Ans_b?></label><br/>
									<input type="radio" id="lidc<?=$a?>" name="ans<?=$a?>" onchange="submitanswer(this.value,<?=$a?>,<?=$timing->ExamId?>);" value="C" data-id="<?=$a?>">
									<label for="lidc<?=$a?>"><?=$value->question->Ans_c?></label><br/>
									<input type="radio" id="lidd<?=$a?>" name="ans<?=$a?>" onchange="submitanswer(this.value,<?=$a?>,<?=$timing->ExamId?>);" value="D" data-id="<?=$a?>">
									<label for="lidd<?=$a?>"><?=$value->question->Ans_d?></label><br/>
								</div>
								<?php $a++;}
								echo LinkPager::widget([
								    'pagination' => $pages,
								]);
								?>
								<input type="hidden" id="endtimeforexam" value="<?=$endtimeforexam?>">		
							</div>
						<!-- <div class="col-3" style="padding-top:10px; background:#efefef;">
							<div style="width:100%; height:auto; float:left; margin-bottom:15px; border-bottom:1px solid #dedede;">
								<button type="button" class="btn btn-circle btn-success btn-xs mb-10">3</button> Answered
								<button type="button" class="btn btn-circle btn-info btn-xs mb-10">4</button> Marked<br/>
								<button type="button" class="btn btn-circle btn-default btn-xs mb-10">2</button> Not Visited
								<button type="button" class="btn btn-circle btn-danger btn-xs mb-10">1</button> Not Answer
							</div>
								<button type="button" class="btn btn-circle btn-success btn-sm mr-5 mb-10">1</button>
								<button type="button" class="btn btn-circle btn-success btn-sm mr-5 mb-10">2</button>
								<button type="button" class="btn btn-circle btn-info btn-sm mr-5 mb-10">3</button>
								<button type="button" class="btn btn-circle btn-success btn-sm mr-5 mb-10">4</button>
								<button type="button" class="btn btn-circle btn-info btn-sm mr-5 mb-10">5</button>
								<button type="button" class="btn btn-circle btn-success btn-sm mr-5 mb-10">6</button>
								<button type="button" class="btn btn-circle btn-danger btn-sm mr-5 mb-10">7</button>
								<button type="button" class="btn btn-circle btn-info btn-sm mr-5 mb-10">8</button>
								<button type="button" class="btn btn-circle btn-default btn-sm mr-5 mb-10">9</button>
								<button type="button" class="btn btn-circle btn-success btn-sm mr-5 mb-10">10</button>
							
							</div> -->
						</div>
					</div>	
					<div class="box-footer">
						<button type="button" class="btn  btn-info mb-5" style="float:right;">Next</button>
					</div>  



				</div>
			</div>
		</div>

	</section>

	<script >
		const second = 1000,
		minute = second * 60,
		hour = minute * 60,
		day = hour * 24;

		let countDown = '<?=$endtime;?>',
		x = setInterval(function() {    
			let now = new Date().getTime(),
			distance = countDown - now;
			document.getElementById('hours').innerText = Math.floor(distance / (hour)),
			document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
			document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

		}, second)
		if(countDown < <?=strtotime(date('Y-m-d H:i:s'))*1000;?>){
			clearInterval(x);
			alert("Times up....");
			window.location.href = '<?=Url::toRoute(['site/index'])?>';
		}

		function submitanswer(value,qid,examid)
		{
			var endexam = $('#endtimeforexam').val();
			$.ajax({url:"<?=Url::toRoute(['site/answer-submit'])?>?ans="+value+"&qid="+qid+"&endexam="+endexam+"&examid="+examid,
				success:function(results)
				{
					if (!results) {
						alert("Oops....Session has Expired");
						window.location.href = window.location.href;
					}
                // alert(results);
            }
        });

		}
	</script>

