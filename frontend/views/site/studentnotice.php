<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Tutorial';
?>
<style>
.mainvideo{border:1px solid #dedede; padding:10px; min-height:250px; overflow:auto;}
.videobox{border:1px solid #dedede; }
</style>
<section class="content">

			<!-- Content Header (Page header) -->	  
			<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Notice</h3>
					</div>
					<div class="w-p60">
						
					</div>
					<div class="right-title w-170">
						<span class="subheader_daterange font-weight-600">
							<span class="subheader_daterange-label">
								<span class="subheader_daterange-title">Today:</span>
								<span class="subheader_daterange-date text-primary"><?=date('M d')?></span>
							</span>
							<a href="#" class="btn btn-rounded btn-sm btn-primary">
								<i class="fa fa-calendar"></i>
							</a>
						</span>
					</div>
				</div>
			</div>
	<div class="row">						
        <div class="col-lg-6 col-12" id="for-search">
		 <div class="box">
			<div class="box-header with-border">
			  <h4 class="box-title">Notice</h4>
			</div>
    		<!-- <div class="col-lg-12" > -->
				<div class="mainvideo">
					<div class="mainvideo">
					<?php if($notices){ $a =1;foreach ($notices as $key => $value) {
						if($value->EndDate >= date('d-m-Y')){
							echo $a.$value->Description;
						}
						else{
							echo '<p>There is no new Notice </p>';
						}
						
					$a++;}}else{ echo '<p>There is no new Notice </p>'; } ?>
				</div>
				</div>
			<!-- </div> -->
		</div>
	</div>
	</div>
</section>

