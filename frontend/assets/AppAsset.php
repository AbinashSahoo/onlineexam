<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
         'css/site.css',
        'css/style.css',
        'css/skin_color.css',
    ];
    public $js = [
	'js/apexcharts.js',
    'js/jquery.flot.js',
    'js/jquery.flot.pie.js',
    'js/template.js',
    'js/demo.js',
    // 'js/pegination.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
