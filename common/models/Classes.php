<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "class".
 *
 * @property int $ClassId
 * @property string $ClassName
 * @property string $SectionName
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Classes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'class';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ClassName'], 'required'],
            [['IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['ClassName', 'SectionName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ClassId' => 'Class ID',
            'ClassName' => 'Class Name',
            'SectionName' => 'Section Name',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }
}
