<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notice".
 *
 * @property int $NoticeId
 * @property int $ClassId
 * @property string $Title
 * @property string $Description
 * @property string $EndDate
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Notice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ClassId', 'Title', 'Description', 'EndDate'], 'required'],
            [['ClassId', 'IsDelete'], 'integer'],
            [['Description'], 'string'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['Title'], 'string', 'max' => 255],
            [['EndDate'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoticeId' => 'Notice ID',
            'ClassId' => 'Class',
            'Title' => 'Title',
            'Description' => 'Description',
            'EndDate' => 'End Date',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }
    public function getClasses()
    {
        return $this->hasOne(Classes::className(), ['ClassId' => 'ClassId']);
    }
}
