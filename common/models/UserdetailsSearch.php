<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Userdetails;

/**
 * UserdetailsSearch represents the model behind the search form of `common\models\Userdetails`.
 */
class UserdetailsSearch extends Userdetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UserDetailsId', 'Classid', 'Professional', 'Trained', 'IsDelete'], 'integer'],
            [['Name', 'Fname', 'Mname', 'CareOf', 'DOB', 'DOJ', 'DOA', 'Nationality', 'Religion', 'Caste', 'Gender', 'PresentAddress', 'PermanentAddress', 'Mobile', 'Whatsapp', 'BloodGroup', 'AdmissionNumber', 'ResgistrationNumber', 'Designation', 'UsertPhoto', 'JointPhoto', 'UserAdhar', 'Fadhar', 'Madhar', 'AdmissionForm', 'BirthCertificate', 'Qualifiaction', 'HighSchoolMarksheet', 'IntermediateMarksheet', 'Graduation', 'Ondate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Userdetails::find()->where(['IsDelete'=>0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'UserDetailsId' => $this->UserDetailsId,
            'DOB' => $this->DOB,
            'DOJ' => $this->DOJ,
            'DOA' => $this->DOA,
            'Classid' => $this->Classid,
            'Professional' => $this->Professional,
            'Trained' => $this->Trained,
            'IsDelete' => $this->IsDelete,
            'Ondate' => $this->Ondate,
            'UpdateDate' => $this->UpdateDate,
        ]);

        $query->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'Fname', $this->Fname])
            ->andFilterWhere(['like', 'Mname', $this->Mname])
            ->andFilterWhere(['like', 'CareOf', $this->CareOf])
            ->andFilterWhere(['like', 'Nationality', $this->Nationality])
            ->andFilterWhere(['like', 'Religion', $this->Religion])
            ->andFilterWhere(['like', 'Caste', $this->Caste])
            ->andFilterWhere(['like', 'Gender', $this->Gender])
            ->andFilterWhere(['like', 'PresentAddress', $this->PresentAddress])
            ->andFilterWhere(['like', 'PermanentAddress', $this->PermanentAddress])
            ->andFilterWhere(['like', 'Mobile', $this->Mobile])
            ->andFilterWhere(['like', 'Whatsapp', $this->Whatsapp])
            ->andFilterWhere(['like', 'BloodGroup', $this->BloodGroup])
            ->andFilterWhere(['like', 'AdmissionNumber', $this->AdmissionNumber])
            ->andFilterWhere(['like', 'ResgistrationNumber', $this->ResgistrationNumber])
            ->andFilterWhere(['like', 'Designation', $this->Designation])
            ->andFilterWhere(['like', 'UsertPhoto', $this->UsertPhoto])
            ->andFilterWhere(['like', 'JointPhoto', $this->JointPhoto])
            ->andFilterWhere(['like', 'UserAdhar', $this->UserAdhar])
            ->andFilterWhere(['like', 'Fadhar', $this->Fadhar])
            ->andFilterWhere(['like', 'Madhar', $this->Madhar])
            ->andFilterWhere(['like', 'AdmissionForm', $this->AdmissionForm])
            ->andFilterWhere(['like', 'BirthCertificate', $this->BirthCertificate])
            ->andFilterWhere(['like', 'Qualifiaction', $this->Qualifiaction])
            ->andFilterWhere(['like', 'HighSchoolMarksheet', $this->HighSchoolMarksheet])
            ->andFilterWhere(['like', 'IntermediateMarksheet', $this->IntermediateMarksheet])
            ->andFilterWhere(['like', 'Graduation', $this->Graduation]);

        return $dataProvider;
    }
}
