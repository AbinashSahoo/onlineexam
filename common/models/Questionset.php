<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "questionset".
 *
 * @property int $questionsetId
 * @property string $SetName
 * @property int $SubFullMark
 * @property int $ClassId
 * @property int $SubjetId
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Questionset extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questionset';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SetName', 'ClassId', 'SubjetId', ], 'required'],
            [['ChapterId', 'ClassId', 'SubjetId', 'IsDelete', 'QuestionId'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['SetName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'questionsetId' => 'Questionset ID',
            'SetName' => 'Exam/Set Name',
            'ChapterId' => 'Chapter Name',
            'QuestionId' => 'Questions',
            'ClassId' => 'Class Name',
            'SubjetId' => 'Subjet Name',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getClasses()
    {
        return $this->hasOne(Classes::className(), ['ClassId' => 'ClassId']);
    }

    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['SubjectId' => 'SubjetId']);
    }

    public function getChapter()
    {
        return $this->hasOne(Chapter::className(), ['ChapterId' => 'ChapterId']);
    }

    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['QuestionId' => 'QuestionId']);
    }

    public function getNoqsn($classid,$subjectid,$setname)
    {
        $qsncnt = Questionset::find()->where(['ClassId'=>$classid,'SubjetId'=>$subjectid,'SetName'=>$setname,'IsDelete'=>0])->count();
        return $qsncnt;
    }

    public function getExamenable()
    {
        return $this->hasOne(Examenable::className(), ['ClassId' => 'ClassId']);
    }

}
