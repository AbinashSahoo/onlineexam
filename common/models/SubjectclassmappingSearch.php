<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Subjectclassmapping;

/**
 * SubjectclassmappingSearch represents the model behind the search form of `common\models\Subjectclassmapping`.
 */
class SubjectclassmappingSearch extends Subjectclassmapping
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SubjectClassId', 'ClassId', 'TeacherId', 'SubjetId', 'IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate','ChapterId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subjectclassmapping::find()->where(['IsDelete'=>0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'SubjectClassId' => $this->SubjectClassId,
            'ClassId' => $this->ClassId,
            'TeacherId' => $this->TeacherId,
            'SubjetId' => $this->SubjetId,
            'IsDelete' => $this->IsDelete,
            'Ondate' => $this->Ondate,
            'UpdateDate' => $this->UpdateDate,
        ]);

        return $dataProvider;
    }
}
