<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $RoleId
 * @property string $RoleName
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RoleName'], 'required'],
            [['IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['RoleName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'RoleId' => 'Role Id',
            'RoleName' => 'Role Name',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }
}
