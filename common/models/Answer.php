<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "answer".
 *
 * @property int $AnswerId
 * @property int $QuestionId
 * @property int $UserId
 * @property string $Answer
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Answer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $ExamId;
    public static function tableName()
    {
        return 'answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['QuestionId', 'UserId', 'Answer'], 'required'],
            [['QuestionId', 'UserId', 'IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['Answer'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AnswerId' => 'Answer ID',
            'QuestionId' => 'Question ID',
            'UserId' => 'User ID',
            'Answer' => 'Answer',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getMarks()
    {
        return $this->hasOne(Question::className(), ['QuestionId' => 'QuestionId','CorrectAns'=>'Answer']);
    }
}
