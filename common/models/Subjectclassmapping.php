<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subjectclassmapping".
 *
 * @property int $SubjectClassId
 * @property int $ClassId
 * @property int $TeacherId
 * @property int $SubjetId
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Subjectclassmapping extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subjectclassmapping';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ClassId', 'TeacherId', 'SubjetId'], 'required'],
            [['ClassId', 'TeacherId', 'SubjetId', 'IsDelete','ChapterId'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SubjectClassId' => 'Subject Class ID',
            'ClassId' => 'Class',
            'TeacherId' => 'Teacher Name',
            'SubjetId' => 'Subjet Name',
            'ChapterId' => 'Chapter Name',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getClasses()
    {
        return $this->hasOne(Classes::className(), ['ClassId' => 'ClassId']);
    }

    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['SubjectId' => 'SubjetId']);
    }

    public function getTeacher()
    {
        return $this->hasOne(User::className(), ['UserId' => 'TeacherId']);
    }

    public function getChapter()
    {
        return $this->hasOne(Chapter::className(), ['ChapterId' => 'ChapterId']);
    }
}
