<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Question;

/**
 * QuestionSearch represents the model behind the search form of `common\models\Question`.
 */
class QuestionSearch extends Question
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['QuestionId', 'QuestionMark', 'ChapterId', 'IsDelete','ClassId','SubjectId'], 'integer'],
            [['Questions', 'Ans_a', 'Ans_b', 'Ans_c', 'Ans_d', 'CorrectAns', 'Ondate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Question::find()->where(['IsDelete'=>0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'QuestionId' => $this->QuestionId,
            'QuestionMark' => $this->QuestionMark,
            'ChapterId' => $this->ChapterId,
            'IsDelete' => $this->IsDelete,
            'Ondate' => $this->Ondate,
            'UpdateDate' => $this->UpdateDate,
        ]);

        $query->andFilterWhere(['like', 'Questions', $this->Questions])
            ->andFilterWhere(['like', 'Ans_a', $this->Ans_a])
            ->andFilterWhere(['like', 'Ans_b', $this->Ans_b])
            ->andFilterWhere(['like', 'Ans_c', $this->Ans_c])
            ->andFilterWhere(['like', 'Ans_d', $this->Ans_d])
            ->andFilterWhere(['like', 'CorrectAns', $this->CorrectAns]);

        return $dataProvider;
    }
}
