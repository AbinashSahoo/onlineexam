<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tutorial".
 *
 * @property int $TutorialId
 * @property int $TeacherId
 * @property int $ClassId
 * @property int $SubjectId
 * @property int $ChapterId
 * @property string $Video
 * @property string $Description
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Tutorial extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tutorial';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TeacherId', 'ClassId', 'SubjectId', 'ChapterId', 'Description','SubjectDoc'], 'required'],
            [['TeacherId', 'ClassId', 'SubjectId', 'ChapterId', 'IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['Video', 'Description'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TutorialId' => 'Tutorial ID',
            'TeacherId' => 'Teacher',
            'ClassId' => 'Class',
            'SubjectId' => 'Subject',
            'SubjectDoc' => 'Topic Pdf',
            'ChapterId' => 'Chapter',
            'Video' => 'Video',
            'Description' => 'Description',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getClasses()
    {
        return $this->hasOne(Classes::className(), ['ClassId' => 'ClassId']);
    }

    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['SubjectId' => 'SubjectId']);
    }

    public function getChapter()
    {
        return $this->hasOne(Chapter::className(), ['ChapterId' => 'ChapterId']);
    }

    public function getTeacher()
    {
        return $this->hasOne(User::className(), ['UserId' => 'TeacherId']);
    }
}
