<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property int $QuestionId
 * @property string $Questions
 * @property string $Ans_a
 * @property string $Ans_b
 * @property string $Ans_c
 * @property string $Ans_d
 * @property string $CorrectAns
 * @property int $QuestionMark
 * @property int $QuestionSetId
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $fullmark;
    public static function tableName()
    {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Questions', 'Ans_a', 'Ans_b', 'Ans_c', 'Ans_d', 'CorrectAns', 'QuestionMark','ClassId','SubjectId'], 'required'],
            [['QuestionMark', 'ChapterId', 'IsDelete','ClassId','SubjectId'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['Questions', 'Ans_a', 'Ans_b', 'Ans_c', 'Ans_d'], 'string', 'max' => 255],
            [['CorrectAns'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'QuestionId' => 'Question ID',
            'Questions' => 'Questions',
            'Ans_a' => 'Ans A',
            'Ans_b' => 'Ans B',
            'Ans_c' => 'Ans C',
            'Ans_d' => 'Ans D',
            'CorrectAns' => 'Correct Ans',
            'QuestionMark' => 'Question Mark',
            'ClassId' => 'Class',
            'SubjectId' => 'Subject',
            'ChapterId' => 'Chapter Name',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getClasses()
    {
        return $this->hasOne(Classes::className(), ['ClassId' => 'ClassId']);
    }

    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['SubjectId' => 'SubjectId']);
    }
    
    public function getChapter()
    {
        return $this->hasOne(Chapter::className(), ['ChapterId' => 'ChapterId']);
    }

    public function getAnswer()
    {
        return $this->hasMany(Answer::className(), ['QuestionId' => 'QuestionId','Answer'=>'CorrectAns']);
    }
    
}
