<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tutorial;

/**
 * TutorialSearch represents the model behind the search form of `common\models\Tutorial`.
 */
class TutorialSearch extends Tutorial
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TutorialId', 'TeacherId', 'ClassId', 'SubjectId', 'ChapterId', 'IsDelete'], 'integer'],
            [['Video', 'Description', 'Ondate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tutorial::find()->where(['IsDelete'=>0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'TutorialId' => $this->TutorialId,
            'TeacherId' => $this->TeacherId,
            'ClassId' => $this->ClassId,
            'SubjectId' => $this->SubjectId,
            'ChapterId' => $this->ChapterId,
            'IsDelete' => $this->IsDelete,
            'Ondate' => $this->Ondate,
            'UpdateDate' => $this->UpdateDate,
        ]);

        $query->andFilterWhere(['like', 'Video', $this->Video])
            ->andFilterWhere(['like', 'Description', $this->Description]);

        return $dataProvider;
    }
}
