<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subject".
 *
 * @property int $SubjectId
 * @property string $SubjectName
 * @property int $Classid
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SubjectName', 'Classid',], 'required'],
            [['Classid','SubFullMark', 'IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['SubjectName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SubjectId' => 'Subject ID',
            'SubjectName' => 'Subject Name',
            'Classid' => 'Class',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getClasses()
    {
        return $this->hasOne(Classes::className(), ['ClassId' => 'Classid']);
    }
}
