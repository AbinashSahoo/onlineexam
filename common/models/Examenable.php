<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "examenable".
 *
 * @property int $ExamId
 * @property int $ClassId
 * @property string $Set
 * @property string $Duration
 * @property string $StartTime
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Examenable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examenable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ClassId','SubjectId', 'Set', 'Duration', 'StartTime'], 'required'],
            [['ClassId', 'IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate','EndTime'], 'safe'],
            [['Set', 'Duration'], 'string', 'max' => 255],
            [['StartTime'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ExamId' => 'Exam ID',
            'ClassId' => 'Class ID',
            'Set' => 'Set',
            'Duration' => 'Duration',
            'StartTime' => 'Start Time',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['SubjectId' => 'SubjectId']);
    }

    public function getQsncnt($classid,$subid)
    {
        $qsncnt = Questionset::find()->where(['ClassId'=>$classid,'SubjetId'=>$subid,'IsDelete'=>0])->count();
        return $qsncnt;
    }
}
