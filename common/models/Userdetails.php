<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "userdetails".
 *
 * @property int $UserDetailsId
 * @property string $Name
 * @property string $Fname
 * @property string $Mname
 * @property string|null $CareOf
 * @property string $DOB
 * @property string|null $DOJ
 * @property string|null $DOA
 * @property string $Nationality
 * @property string $Religion
 * @property string $Caste
 * @property string $Gender
 * @property string|null $PresentAddress
 * @property string|null $PermanentAddress
 * @property string $Mobile
 * @property string $Whatsapp
 * @property int|null $Classid
 * @property string $BloodGroup
 * @property string $AdmissionNumber
 * @property string $ResgistrationNumber
 * @property string $Designation
 * @property int|null $Professional 0=no,1=yes
 * @property int|null $Trained 0=no,1=yes
 * @property string $UsertPhoto
 * @property string|null $JointPhoto
 * @property string $UserAdhar
 * @property string|null $Fadhar
 * @property string|null $Madhar
 * @property string|null $AdmissionForm
 * @property string|null $BirthCertificate
 * @property string|null $Qualifiaction
 * @property string|null $HighSchoolMarksheet
 * @property string|null $IntermediateMarksheet
 * @property string|null $Graduation
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Userdetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userdetails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Name', 'Fname', 'Mname', 'DOB', 'Nationality', 'Religion', 'Caste', 'Gender', 'Mobile', 'Whatsapp', 'BloodGroup', 'ResgistrationNumber'], 'required'],
            [['DOB', 'DOJ', 'DOA', 'Ondate', 'UpdateDate'], 'safe'],
            [['Classid', 'Professional', 'Trained', 'IsDelete'], 'integer'],
            [['Name', 'CareOf', 'Nationality', 'Religion', 'Caste', 'Gender', 'PresentAddress', 'PermanentAddress', 'Mobile', 'Whatsapp', 'BloodGroup', 'UsertPhoto', 'JointPhoto', 'UserAdhar', 'Fadhar', 'Madhar', 'BirthCertificate'], 'string', 'max' => 200],
            [['Fname', 'Mname', 'AdmissionNumber', 'ResgistrationNumber', 'Designation', 'AdmissionForm', 'Qualifiaction', 'HighSchoolMarksheet', 'IntermediateMarksheet', 'Graduation'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UserDetailsId' => 'User Details ID',
            'Name' => 'Name',
            'Fname' => 'Father Name',
            'Mname' => 'Mother Name',
            'CareOf' => 'Care Of',
            'DOB' => 'Date Of Birth',
            'DOJ' => 'Date Of Join',
            'DOA' => 'Date Of Admission',
            'Nationality' => 'Nationality',
            'Religion' => 'Religion',
            'Caste' => 'Caste',
            'Gender' => 'Gender',
            'PresentAddress' => 'Present Address',
            'PermanentAddress' => 'Permanent Address',
            'Mobile' => 'Mobile',
            'Whatsapp' => 'Whatsapp',
            'Classid' => 'Class',
            'BloodGroup' => 'Blood Group',
            'AdmissionNumber' => 'Admission Number',
            'ResgistrationNumber' => 'Resgistration Number',
            'Designation' => 'Designation',
            'Professional' => 'Professional',
            'Trained' => 'Trained',
            'UsertPhoto' => 'User Photo',
            'JointPhoto' => 'Joint Photo',
            'UserAdhar' => 'User Aadhar',
            'Fadhar' => 'Father Aadhar',
            'Madhar' => 'Mother Aadhar',
            'AdmissionForm' => 'Admission Form',
            'BirthCertificate' => 'Birth Certificate',
            'Qualifiaction' => 'Qualifiaction',
            'HighSchoolMarksheet' => 'High School Marksheet',
            'IntermediateMarksheet' => 'Intermediate Marksheet',
            'Graduation' => 'Graduation',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getClasses()
    {
        return $this->hasOne(Classes::className(), ['ClassId' => 'Classid']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['UserDetailsId' => 'UserDetailsId']);
    }

}
