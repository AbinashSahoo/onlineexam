<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property int $DocumentId
 * @property int $UserDetailsId
 * @property string $DocumentName
 * @property string $Document
 * @property string|null $DocumentValue
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UserDetailsId', 'DocumentName', 'Document'], 'required'],
            [['UserDetailsId', 'IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['DocumentName', 'Document'], 'string', 'max' => 255],
            [['DocumentValue'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DocumentId' => 'Document ID',
            'UserDetailsId' => 'User Details ID',
            'DocumentName' => 'Document Name',
            'Document' => 'Document',
            'DocumentValue' => 'Document Value',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }
}
