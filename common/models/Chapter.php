<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "chapter".
 *
 * @property int $ChapterId
 * @property string $ChapterName
 * @property int $ClassId
 * @property int $SubjectId
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Chapter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chapter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ChapterName', 'ClassId', 'SubjectId'], 'required'],
            [['ClassId', 'SubjectId', 'IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
            [['ChapterName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ChapterId' => 'Chapter ID',
            'ChapterName' => 'Chapter Name',
            'ClassId' => 'Class',
            'SubjectId' => 'Subject',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }

    public function getClasses()
    {
        return $this->hasOne(Classes::className(), ['ClassId' => 'ClassId']);
    }

    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['SubjectId' => 'SubjectId']);
    }
}
