<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "result".
 *
 * @property int $ResultId
 * @property int $StudentMark
 * @property int $SubjectId
 * @property int $StudentId
 * @property int $QuestionSetId
 * @property int $IsDelete 0=NotDeleted,1=Deleted
 * @property string|null $Ondate
 * @property string|null $UpdateDate
 */
class Result extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['StudentMark', 'SubjectId', 'StudentId', 'QuestionSetId'], 'required'],
            [['StudentMark', 'SubjectId', 'StudentId', 'QuestionSetId', 'IsDelete'], 'integer'],
            [['Ondate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ResultId' => 'Result ID',
            'StudentMark' => 'Student Mark',
            'SubjectId' => 'Subject ID',
            'StudentId' => 'Student ID',
            'QuestionSetId' => 'Question Set ID',
            'IsDelete' => 'Is Delete',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
        ];
    }
}
