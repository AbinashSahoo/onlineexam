<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Questionset;

/**
 * QuestionsetSearch represents the model behind the search form of `common\models\Questionset`.
 */
class QuestionsetSearch extends Questionset
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['questionsetId', 'ChapterId', 'ClassId', 'SubjetId', 'IsDelete'], 'integer'],
            [['SetName', 'Ondate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$classid ='',$subjectid ='',$chapterid ='')
    {
        if($classid != '' && $subjectid != '' && $chapterid != ''){
            $query = Questionset::find()->where(['ClassId' => $classid ,'SubjetId' => $subjectid,'ChapterId' => $chapterid ,'IsDelete'=>0])->groupBy(['ClassId', 'SubjetId', 'SetName']);   
        }else{
        $query = Questionset::find()->where(['IsDelete'=>0])->groupBy(['ClassId', 'SubjetId', 'SetName']);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'questionsetId' => $this->questionsetId,
            'ChapterId' => $this->ChapterId,
            'ClassId' => $this->ClassId,
            'SubjetId' => $this->SubjetId,
            'IsDelete' => $this->IsDelete,
            'Ondate' => $this->Ondate,
            'UpdateDate' => $this->UpdateDate,
        ]);

        $query->andFilterWhere(['like', 'SetName', $this->SetName]);

        return $dataProvider;
    }
}
