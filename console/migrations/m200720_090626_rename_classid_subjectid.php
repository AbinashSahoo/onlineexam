<?php

use yii\db\Migration;

/**
 * Class m200720_090626_rename_classid_subjectid
 */
class m200720_090626_rename_classid_subjectid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('question', 'SubjectId', 'QuestionMark');
        $this->renameColumn('question', 'ClassId', 'QuestionSetId');
        $this->renameColumn('question', 'Qid', 'QuestionId');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_090626_rename_classid_subjectid cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_090626_rename_classid_subjectid cannot be reverted.\n";

        return false;
    }
    */
}
