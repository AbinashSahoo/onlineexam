<?php

use yii\db\Migration;

/**
 * Class m200728_055130_tutorial_table
 */
class m200728_055130_tutorial_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tutorial}}', [
            'TutorialId' => $this->primaryKey(),
            'TeacherId' => $this->integer()->notNull(),
            'ClassId'=> $this->integer()->notNull(),
            'SubjectId' => $this->integer()->notNull(),
            'ChapterId' => $this->integer()->notNull(),
            'Video' => $this->string(250)->notNull(),
            'SubjectDoc' => $this->string(250)->notNull(),
            'Description' => $this->string(250)->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200728_055130_tutorial_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200728_055130_tutorial_table cannot be reverted.\n";

        return false;
    }
    */
}
