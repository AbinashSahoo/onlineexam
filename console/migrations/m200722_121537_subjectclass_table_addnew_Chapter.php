<?php

use yii\db\Migration;

/**
 * Class m200722_121537_subjectclass_table_addnew_Chapter
 */
class m200722_121537_subjectclass_table_addnew_Chapter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%subjectclassmapping%}}','ChapterId',$this->integer()->Null()->after('SubjetId'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200722_121537_subjectclass_table_addnew_Chapter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200722_121537_subjectclass_table_addnew_Chapter cannot be reverted.\n";

        return false;
    }
    */
}
