<?php

use yii\db\Migration;

/**
 * Class m200720_092144_Result_table
 */
class m200720_092144_Result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%result}}', [
            'ResultId' => $this->primaryKey(),
            'StudentMark' => $this->integer()->notNull(),
            'SubjectId'=> $this->integer()->notNull(),
            'StudentId' => $this->integer()->notNull(),
            'QuestionSetId' => $this->integer()->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_092144_Result_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_092144_Result_table cannot be reverted.\n";

        return false;
    }
    */
}
