<?php

use yii\db\Migration;

/**
 * Class m200722_121047_question_table_rename_Chapter
 */
class m200722_121047_question_table_rename_Chapter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('question', 'QuestionSetId', 'ChapterId');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200722_121047_question_table_rename_Chapter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200722_121047_question_table_rename_Chapter cannot be reverted.\n";

        return false;
    }
    */
}
