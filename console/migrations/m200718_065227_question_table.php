<?php

use yii\db\Migration;

/**
 * Class m200718_065227_question_table
 */
class m200718_065227_question_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%question}}', [
            'Qid' => $this->primaryKey(),
            'Questions' => $this->string()->notNull(),
            'Ans_a'=> $this->string()->notNull(),
            'Ans_b' => $this->string()->notNull(),
            'Ans_c' => $this->string()->notNull(),
            'Ans_d'=> $this->string()->notNull(),
            'CorrectAns' => $this->string(1)->notNull(),
            'SubjectId' => $this->integer()->notNull(),
            'ClassId'=> $this->integer()->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200718_065227_question_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200718_065227_question_table cannot be reverted.\n";

        return false;
    }
    */
}
