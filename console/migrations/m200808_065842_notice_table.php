<?php

use yii\db\Migration;

/**
 * Class m200808_065842_notice_table
 */
class m200808_065842_notice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%notice}}', [
            'NoticeId' => $this->primaryKey(),
            'ClassId' => $this->integer()->notNull(),
            'Title' => $this->string()->notNull(50),
            'Description'=> $this->text()->notNull(),
            'EndDate'=> $this->string(50)->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200808_065842_notice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200808_065842_notice_table cannot be reverted.\n";

        return false;
    }
    */
}
