<?php

use yii\db\Migration;

/**
 * Class m200718_065244_answer_table
 */
class m200718_065244_answer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%answer}}', [
            'AnswerId' => $this->primaryKey(),
            'QuestionId' => $this->integer()->notNull(),
            'UserId'=> $this->integer()->notNull(),
            'Answer' => $this->string(1)->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200718_065244_answer_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200718_065244_answer_table cannot be reverted.\n";

        return false;
    }
    */
}
