<?php

use yii\db\Migration;

/**
 * Class m200729_104910_document_table
 */
class m200729_104910_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document}}', [
            'DocumentId' => $this->primaryKey(),
            'UserDetailsId' => $this->integer()->notNull(),
            'DocumentName' => $this->string()->notNull(200),
            'Document'=> $this->string()->notNull(200),
            'DocumentValue'=> $this->string(200)->Null(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200729_104910_document_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200729_104910_document_table cannot be reverted.\n";

        return false;
    }
    */
}
