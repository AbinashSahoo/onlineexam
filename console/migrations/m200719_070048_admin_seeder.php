<?php

use yii\db\Migration;

/**
 * Class m200719_070048_admin_seeder
 */
class m200719_070048_admin_seeder extends Migration
{
    private $tablename='user'; 
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $username=[array('UserName'=>'Admin','Password'=>'$2y$13$Xvi4bXl9oolz6Hbl9vJPSuWqPL6YaDFOVYe/7KZyn0pZUDmS.AMMy','RoleId'=>1)];
        foreach ($username as $user) {
            $this->insert($this->tablename,$user);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200719_070048_admin_seeder cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200719_070048_admin_seeder cannot be reverted.\n";

        return false;
    }
    */
}
