<?php

use yii\db\Migration;

/**
 * Class m200720_092112_QuestinsSet_table
 */
class m200720_092112_QuestinsSet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%questionset}}', [
            'questionsetId' => $this->primaryKey(),
            'SetName' => $this->string()->notNull(),
            'ClassId' => $this->integer()->notNull(),
            'SubjetId' => $this->integer()->notNull(),
            'ChapterId' => $this->integer()->notNull(),
            'QuestionId' => $this->integer()->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_092112_QuestinsSet_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_092112_QuestinsSet_table cannot be reverted.\n";

        return false;
    }
    */
}
