<?php

use yii\db\Migration;

/**
 * Class m200725_060339_question_table_addnew_Columns
 */
class m200725_060339_question_table_addnew_Columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%question%}}','ClassId',$this->integer()->Null()->after('QuestionMark'));
        $this->addColumn('{{%question%}}','SubjectId',$this->integer()->Null()->after('ClassId'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200725_060339_question_table_addnew_Columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200725_060339_question_table_addnew_Columns cannot be reverted.\n";

        return false;
    }
    */
}
