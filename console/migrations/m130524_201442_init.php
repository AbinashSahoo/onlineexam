<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'UserId' => $this->primaryKey(),
            'UserName' => $this->string()->notNull()->unique(),
            'Password' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'RoleId' => $this->integer()->notNull(),
            'UserDetailsId' => $this->integer()->notNull(),
            'IsDelete'=>$this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()'),
            'auth_key' => $this->string(32)->Null(),

            'status' => $this->smallInteger()->Null()->defaultValue(0),
            'created_at' => $this->integer()->Null(),
            'updated_at' => $this->integer()->Null(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
