<?php

use yii\db\Migration;

/**
 * Class m200722_121840_new_table_chapter
 */
class m200722_121840_new_table_chapter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chapter}}', [
            'ChapterId' => $this->primaryKey(),
            'ChapterName' => $this->string()->notNull(),
            'ClassId'=> $this->integer()->notNull(),
            'SubjectId' => $this->integer()->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200722_121840_new_table_chapter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200722_121840_new_table_chapter cannot be reverted.\n";

        return false;
    }
    */
}
