<?php

use yii\db\Migration;

/**
 * Class m200722_120420_Subject_table_newadd_fullmark
 */
class m200722_120420_Subject_table_newadd_fullmark extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('subject', 'SubFullMark', 'INT after IsDelete');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200722_120420_Subject_table_newadd_fullmark cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200722_120420_Subject_table_newadd_fullmark cannot be reverted.\n";

        return false;
    }
    */
}
