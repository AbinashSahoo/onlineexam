<?php

use yii\db\Migration;

/**
 * Class m200720_091539_SubjectClass_mapping_table
 */
class m200720_091539_SubjectClass_mapping_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%subjectclassmapping}}', [
            'SubjectClassId' => $this->primaryKey(),
            'ClassId' => $this->integer()->notNull(),
            'TeacherId'=> $this->integer()->notNull(),
            'SubjetId' => $this->integer()->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_091539_SubjectClass_mapping_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_091539_SubjectClass_mapping_table cannot be reverted.\n";

        return false;
    }
    */
}
