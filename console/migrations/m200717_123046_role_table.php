<?php

use yii\db\Migration;

/**
 * Class m200717_123046_role_table
 */
class m200717_123046_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->createTable('{{%role}}', [
            'RoleId' => $this->primaryKey(),
            'RoleName' => $this->string()->notNull(),
            'IsDelete'=>$this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200717_123046_role_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200717_123046_role_table cannot be reverted.\n";

        return false;
    }
    */
}
