<?php

use yii\db\Migration;

/**
 * Class m200801_064234_Examenable_table
 */
class m200801_064234_Examenable_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%examenable}}', [
            'ExamId' => $this->primaryKey(),
            'ClassId' => $this->integer()->notNull(),
            'SubjectId' => $this->integer()->notNull(),
            'Set' => $this->string()->notNull(50),
            'Duration'=> $this->string()->notNull(50),
            'StartTime'=> $this->string(50)->notNull(),
            'EndTime'=> $this->string(50)->notNull(),
            'Status' => $this->boolean()->notNull()->defaultValue(0)->comment("0=enabled,1=disabled"),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200801_064234_Examenable_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200801_064234_Examenable_table cannot be reverted.\n";

        return false;
    }
    */
}
