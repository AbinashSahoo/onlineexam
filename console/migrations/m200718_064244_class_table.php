<?php

use yii\db\Migration;

/**
 * Class m200718_064244_class_table
 */
class m200718_064244_class_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%class}}', [
            'ClassId' => $this->primaryKey(),
            'ClassName' => $this->string()->notNull(),
            'SectionName'=> $this->string()->notNull(),
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200718_064244_class_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200718_064244_class_table cannot be reverted.\n";

        return false;
    }
    */
}
