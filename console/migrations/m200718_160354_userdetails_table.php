<?php

use yii\db\Migration;

/**
 * Class m200718_160354_userdetails_table
 */
class m200718_160354_userdetails_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%userdetails}}', [
            'UserDetailsId' => $this->primaryKey(),
            'Name' => $this->string(200)->notNull(),
            'Fname' => $this->string()->notNull(200),
            'Mname'=> $this->string()->notNull(200),
            'CareOf'=> $this->string(200)->Null(),
            'DOB' => $this->date()->notNull(),
            'DOJ'=> $this->date()->Null(),
            'DOA'=> $this->date()->Null(),
            'Nationality'=> $this->string(200)->notNull(),
            'Religion'=> $this->string(200)->notNull(),
            'Caste'=> $this->string(200)->notNull(),
            'Gender'=> $this->string(200)->notNull(),
            'PresentAddress' => $this->string(200)->Null(),
            'PermanentAddress' => $this->string(200)->Null(),
            'Mobile' => $this->string(200)->notNull(),
            'Whatsapp' => $this->string(200)->notNull(),
            'Classid'=> $this->integer()->Null(),
            'BloodGroup'=> $this->string(200)->notNull(),
            'AdmissionNumber' => $this->string()->notNull(200),
            'ResgistrationNumber'=> $this->string()->notNull(200),
            'Designation' => $this->string()->notNull(200),
            'Professional'=> $this->boolean()->Null()->comment("0=no,1=yes"),
            'Trained'=> $this->boolean()->Null()->comment("0=no,1=yes"),

            'UsertPhoto'=> $this->string(200)->notNull(),
            'JointPhoto' => $this->string(200)->Null(),
            'UserAdhar' => $this->string(200)->notNull(),
            'Fadhar' => $this->string(200)->Null(),
            'Madhar' => $this->string(200)->Null(),
            'AdmissionForm'=> $this->string()->Null(),
            'BirthCertificate'=> $this->string(200)->Null(),
            'Qualifiaction' => $this->string()->Null(200),
            'HighSchoolMarksheet'=> $this->string()->Null(200),
            'IntermediateMarksheet' => $this->string()->Null(200),
            'Graduation'=> $this->string()->Null(200),
            
            'IsDelete' => $this->boolean()->notNull()->defaultValue(0)->comment("0=NotDeleted,1=Deleted"),
            'Ondate' => $this->dateTime()->defaultExpression('NOW()'),
            'UpdateDate' => $this->dateTime()->defaultExpression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200718_160354_userdetails_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200718_160354_userdetails_table cannot be reverted.\n";

        return false;
    }
    */
}
