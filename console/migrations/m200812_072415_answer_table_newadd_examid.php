<?php

use yii\db\Migration;

/**
 * Class m200812_072415_answer_table_newadd_examid
 */
class m200812_072415_answer_table_newadd_examid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('answer', 'ExamId', 'INT after UserId');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200812_072415_answer_table_newadd_examid cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200812_072415_answer_table_newadd_examid cannot be reverted.\n";

        return false;
    }
    */
}
