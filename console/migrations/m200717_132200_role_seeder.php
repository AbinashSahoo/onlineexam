<?php

use yii\db\Migration;

/**
 * Class m200717_132200_role_seeder
 */
class m200717_132200_role_seeder extends Migration
{
    private $tablename = 'role';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $role = [
            array('RoleId' => 1, 'RoleName' => 'Admin'),
            array('RoleId' => 2, 'RoleName' => 'Teacher'),
            array('RoleId' => 3, 'RoleName' => 'Student'),
        ];
        foreach($role as $roles){
            $this->insert($this->tablename,$roles);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200717_132200_role_seeder cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200717_132200_role_seeder cannot be reverted.\n";

        return false;
    }
    */
}
